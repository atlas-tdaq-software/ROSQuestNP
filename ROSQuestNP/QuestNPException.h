/*******************************************************************/
/*                                                                 */
/* This is the header file of the QuestNPException class           */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/**C 2018 Ecosoft - Made from at least 80% recycled source code*****/

#ifndef QUESTNPEXCEPTION_H
#define QUESTNPEXCEPTION_H

#include <stdexcept>
#include <sstream>
#include <string>
#include <iostream>
#include <sys/types.h>


//Macro
#define THROW_QUESTNP_EXCEPTION(errorCode, message) \
  { std::ostringstream oss; \
    oss << message; \
    throw QuestNPException(QuestNPException::errorCode, oss.str()); \
  }
      
 
class QuestNPException : public std::runtime_error 
{
  public:  

    enum
    {
      IORCC = 1,
      I2C,
      QSFP,
      VCO,
      ALARM,
      TIMEOUT,
      SIZE_ERROR,
      NO_CODE
    };

    virtual const std::string getDescription() const;
    u_int getErrorId() const;
    virtual ~QuestNPException() throw () {} 

    QuestNPException(u_int errorId, std::string errorText);

  protected:
     virtual std::string getErrorString(u_int errorId) const;

  private:
    const u_int m_errorId;
    const std::string m_errorText;
    virtual std::ostream & print(std::ostream &stream) const;    
    std::ostream & printErrorMessage(std::ostream &stream) const;    
    std::ostream & printDescription(std::ostream &stream) const;    
};


inline std::ostream & QuestNPException::print(std::ostream &stream) const 
{
  return printErrorMessage(stream);  
}

#endif //QUESTNPEXCEPTION_H
