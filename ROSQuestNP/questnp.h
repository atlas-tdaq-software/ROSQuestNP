/************************************************************************/
/*									*/
/* File: questnp.h							*/
/*									*/
/* This is the public header file for QUESTNP library			*/
/*									*/
/* 29. Jun. 18  MAJO  created						*/
/*									*/
/************ C 2018 - The software with that certain something *********/

#ifndef _QUESTNP_H 
#define _QUESTNP_H

#include <linux/types.h>


/*************/
/* Constants */
/*************/
#define I2C_CHAIN_CLOCK       0x1
#define I2C_CHAIN_QSFP_0      0x8
#define I2C_CHAIN_QSFP_1      0x9
#define I2C_CHAIN_QSFP_2      0xa

#define I2C_SLAVE_CLOCK       0x55
#define I2C_SLAVE_QSFP_0      0x50
#define I2C_SLAVE_QSFP_1      0x50
#define I2C_SLAVE_QSFP_2      0x50

#define I2C_VENDOR_START      148
#define I2C_PART_START        168
#define I2C_REVISION_START    184
#define I2C_SERIAL_START      196
#define I2C_MANUFACTURE_START 212
#define I2C_TEMPERATURE       22
#define I2C_VOLTAGE           26
#define I2C_POWER_START       34
#define I2C_BIAS_START        42
#define I2C_ERROR_LOSS_START  3    
#define I2C_SECOND_PAGE       128

#define M_RECALL              (1<<0)
#define M_NEWFREQ             (1<<6)
#define FREEZE_DCO            (1<<4)

#define MAX_TX_FIFO_FILL      510      //Maximum number of S-Link control or data words that can be send to the FIFO in the F/W
#define MASK_TX_FIFO_FILL     0xfff    //This defines the number of bits on the TX_FIFO_FILL bit field


/*******/
/*Types*/
/*******/
typedef struct 
{
  volatile u_int cw_port_a[1024];          /*0x0000*/
  volatile u_int data_port_a32[512];       /*0x1000*/  
  volatile u_long data_port_a64[256];      /*0x1800*/
  volatile u_int cw_port_b[1024];          /*0x2000*/
  volatile u_long csr_low;                 /*0x3000*/
  volatile u_long csr_high;                /*0x3008*/
  volatile u_long csr_dummy[254];          /*0x3010*/
  volatile u_long dma[256];                /*0x3800*/
} T_questnp_channel;

typedef struct
{
  volatile u_long     design_id_l;          /*0x00000*/
  volatile u_long     design_id_h;          /*0x00008*/
  volatile u_long     gcr;                  /*0x00010*/
  volatile u_long     gsr;                  /*0x00018*/
  volatile u_int      icr;                  /*0x00020*/
  volatile u_int      imr;                  /*0x00024*/
  volatile u_long     isr;                  /*0x00028*/
  volatile u_long     i2csc;                /*0x00030*/
  volatile u_char     dummy1[0xffc8];       /*0x00038*/
  T_questnp_channel   channel[12];          /*0x10000*/
} T_questnp_regs;

typedef struct 
{
  u_char vendor_name[17];
  u_char part_number[17];
  u_char revision[3];
  u_char serial_number[17];
  u_char manufacture_date[11];
  u_char temperature;
  float voltage;
  float rx_power[4];
  float tx_bias[4];
  u_char tx_error[4];
  u_char loss_of_tx_signal[4];
  u_char loss_of_rx_signal[4];
} T_qsfp_monitoring_data;

typedef struct 
{
  u_int n1;
  u_int hs_div;
  u_long rfreq_int;
  double fxtal;
} T_refclk_cfg_t;

typedef struct 
{
  u_int fifo_full_counter;
  u_int data_transfer_ratio;
  u_int link_up;
  u_int fifo_full;
  u_int link_down_count;
  u_int xoff;
  u_int rx_sync;
  u_int gtx_reset_done;
  u_int rx_pll_locked;
  u_int tx_fifo_fill;
  u_int channel_number;
  u_long rawdata;
} T_quest_channel_config;


typedef struct 
{
  u_int minor;
  u_int major;
  u_int version;
  u_int ncps;
  u_int nsubquests;
  u_int nchanels;
  T_quest_channel_config channel[12];
} T_quest_config;


/************/
/*Prototypes*/
/************/
class QuestNP
{
  public:
    QuestNP();

    /*********************************/ 
    /* Official functions of the API */
    /*********************************/ 
    void card_open(int occ);
    void card_close(void);
    void send_packet(u_int channel, u_int size, const u_int *source);
    void send_packet_memcpy(u_int channel, u_int size, const u_int *source);
    u_char i2c_read(u_char slave, u_char device, u_char address);
    void i2c_write(u_char slave, u_char device, u_char address, u_char data);
    void i2c_double_write(u_char slave, u_char device, u_char address1, u_char data1, u_char address2, u_char data2);
    void get_QSFP_data(u_int qsfp, T_qsfp_monitoring_data *qsfp_data);
    void get_config(T_quest_config *qsfp_config);
    void configure_i2c_clock(float newFreq);
    void slink_reset(u_int channel);
    void channel_reset(u_int channel);

    /********************/
    /* Object variables */
    /********************/
    u_long m_sreg;
    T_questnp_regs *m_questnp;
    T_refclk_cfg_t m_cfg;

  private:
    void waitForClockClearance(u_char flag);
    void getClockConfig(float fout);
    void computeClockConfig(float newFrequency);
    void applyClockConfig();
};

#endif

