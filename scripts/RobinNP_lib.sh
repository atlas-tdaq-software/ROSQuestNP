#!/bin/bash
MAILTO=${MAILTO:-markus.joos@cern.ch}
#BASEDIR=/det/tdaq/RobinNP/$HOSTNAME
#RNPDIR=/daq_area/tools/ros/Grape/scripts/RobinNP
BASEDIR=/dsk1/quest/grape_quest/$HOSTNAME
RNPDIR=/dsk1/quest/grape_quest/RobinNP

MYNAME=$(basename $0)
## check the path
cd $RNPDIR || exit 1
[ -x pull_progb.sh ] || exit 1

## prepare for logs etc
mkdir -p $BASEDIR
chmod 775 $BASEDIR
LOG1=$BASEDIR/$(date +%F_%X)_progstep1.log
DONE1=$BASEDIR/done.step1
LOG2=$BASEDIR/$(date +%F_%X)_progstep2.log
DONE2=$BASEDIR/done.step2

function log {
	echo "## $MYNAME: $@"
	logger -t $MYNAME "$@"
}

function robinnp_count {
local n1=$(/sbin/lspci -n | grep -e "10dc:0187" -e "10dc:01aa" -e "10dc:01a0" | wc -l)
echo $[n1]
}
