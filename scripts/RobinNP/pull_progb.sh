#!/bin/bash
#
# Heiko Engel, <hengel@cern.ch>
#
# Changelog:
# 2013-07-26 - HE - initial checkin
# 2013-07-31 - HE - added flash selection
#
# Usage:
#       ./pull_progb.sh [slvaddr] ([flashsel])
# [slvaddr] : (required) The slave address of the microcontroller.
# [flashsel]: (optional) Selects the flash from where the device will
#             be reconfigured. 
#             Possible values are 0 (->FLASH0) and 1 (FLASH1).
# Example:
# ./pull_progb.sh 0x44 0

if [ -z $1 ]; then
  echo "please provide slave address as argument"
  exit
fi

# use first argument as slave address
SLV=$1

# I2C Chain: adjust this to your system. 
# This corresponds to /dev/i2c-[CHAIN], 
# CHAIN=0 for /dev/i2c-0, CHAIN=1 for /dev/i2c-1, etc.
CHAIN=2

# uC Firmware Register Mappings
PINC=0x0b
PORTC=0x11
DDRC=0x10

PIND=0x0c
PORTD=0x13
DDRD=0x12

if [ -z $2 ]; then
  # no second argument:
  # drive only uc_FPGA_PROG_B
  DDRCFG=0x20
  # PROG_B low
  VALCFG=0x00
else
  # there is a second argument:
  # drive PROG_B and FLASH_A23
  DDRCFG=0x30
  if [ $2 -eq 1 ]; then
    # driving FLASH_A23=0 selects FLASH1
    # PROG_B low, FLASH_A23 low
    VALCFG=0x00
  else
    # driving FLASH_A23=1 selects FLASH0
    # PROG_B low, FLASH_A23 high
    VALCFG=0x10
  fi

fi

PIN=$PINC
PORT=$PORTC
DDR=$DDRC


function status {
echo "==Status=="
echo CHAIN=$CHAIN
echo SLV=$SLV
echo PIN=$PIN
echo PORT=$PORT
echo DDR=$DDR
# get current PINC
echo "PINC="`sudo ./i2cget -y $CHAIN $SLV $PIN`
# get current PORTC
echo "PORTC="`sudo ./i2cget -y $CHAIN $SLV $PORT`
# get current DDRC
echo "DDRC="`sudo ./i2cget -y $CHAIN $SLV $DDR`
echo ""
}

status

# drive PINs
sudo ./i2cset -y $CHAIN $SLV $PORT $VALCFG
# set to output
sudo ./i2cset -y $CHAIN $SLV $DDR $DDRCFG

status 

sleep 1

# set PROG_B to input again, leave FLASH_A23 as output if 
# second argument was selected
if [ -z $2 ]; then
  sudo ./i2cset -y $CHAIN $SLV $DDR 0x00
  # disable pullups
  sudo ./i2cset -y $CHAIN $SLV $PORT 0x00
else
  sudo ./i2cset -y $CHAIN $SLV $DDR 0x10
  # TODO: what about pullups here?!
fi


status

