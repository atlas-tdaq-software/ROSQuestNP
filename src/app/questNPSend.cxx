#include <iostream>
#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <chrono>
#include <sys/stat.h>


#include "questNPSendIssues.h"

#include "QuestNPInfo/QuestNPSenderInfo.h"
#include "QuestNPInfo/QuestNPInfo.h"

#include "ROSQuestNP/questnp.h"

#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlCommands.h"

#include "dal/MasterTrigger.h"
#include "TriggerCommander/MasterTrigger.h"
#include "TriggerCommander/CommandedTrigger.h"
#include "TriggerCommander/HoldTriggerInfo.h"

#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/FSM/FSMCommands.h"

#include "is/infodictionary.h"
#include "is/exceptions.h"

#include "config/Configuration.h"
#include "dal/util.h"
#include "dal/Partition.h"
#include "dal/Component.h"
#include "dal/Detector.h"

#include "DFdal/DataFile.h"
#include "DFdal/DFParameters.h"

#include "DFdal/QuestNPSender.h"
#include "DFdal/QuestNPModule.h"
#include "DFdal/QuestNPChannel.h"

#include "eformat/eformat.h"
#include "eformat/write/eformat.h"
#include "eformat/old/util.h"
#include "EventStorage/pickDataReader.h"


namespace questnpsend{

   class QuestNPSender : public daq::rc::Controllable, public daq::trigger::MasterTrigger {
   public:
      QuestNPSender(const std::string name,bool);
      virtual ~QuestNPSender();
      void configure(const daq::rc::TransitionCmd&);
      void unconfigure(const daq::rc::TransitionCmd&);
      void prepareForRun(const daq::rc::TransitionCmd&);
      void stopDC(const daq::rc::TransitionCmd&);
      void publish();

      daq::trigger::HoldTriggerInfo hold(const std::string& dm) override;
      void resume(const std::string& dm);
      void increaseLumiBlock(uint32_t runno);
      void setLumiBlockInterval(uint32_t seconds);
      void setMinLumiBlockLength(uint32_t seconds);
      void setPrescales(uint32_t l1p, uint32_t hltp){}
      void setL1Prescales(uint32_t l1p){}
      void setHLTPrescales(uint32_t hltp){}
      void setBunchGroup(uint32_t bg){}
      void setConditionsUpdate(uint32_t folderIndex, uint32_t lb){}
      void setPrescalesAndBunchgroup(uint32_t l1p, uint32_t hltp, uint32_t bg) {}

   private:
      void unconfigure();
      void unpackFragments(
         const eformat::FullEventFragment<const uint32_t*>& event);
      void loadFile(const std::string& fileSpec);
      void clearStats();
      void initStats(unsigned int chan);
      void sendLoop(unsigned int modnum,unsigned int channum);
      std::mutex m_statsMutex;
      std::string m_name;
      std::string m_partitionName;
      IPCPartition* m_ipcPartition;
      std::string m_isInfoName;
      ISInfoDictionary m_dictionary;

      std::vector<std::vector<unsigned int>> m_chanIds;
      std::vector<std::vector<unsigned int>> m_chanAddr;
      std::vector<QuestNP*> m_quest;

      std::vector<std::thread> m_thread;

      typedef std::map<unsigned int, uint32_t*> EventMap;
      std::map<unsigned int, EventMap> m_store;
      std::set<unsigned int> m_rolIdList;
      unsigned int m_storeSize;
      unsigned int m_maxLevel1;
      unsigned int m_maxFragSize;

      std::vector<std::string> m_questInfoName;
      std::vector<QuestNPInfo> m_questInfo;
      QuestNPSenderInfo m_isInfo;

      unsigned int m_startDelay;
      unsigned int m_delayBetweenEvents;
      bool m_runActive;
      bool m_triggerHeld;
      bool m_interactive;
      bool m_configured;
      bool m_replaceL1Id;

      daq::trigger::CommandedTrigger* m_commandedTrigger;
   };
}

using namespace questnpsend;


QuestNPSender::QuestNPSender(const std::string name,bool interactive)
   : m_name(name),
     m_ipcPartition(0),
     m_maxFragSize(0),
     m_startDelay(0),
     m_delayBetweenEvents(0),
     m_triggerHeld(true),
     m_interactive(interactive),
     m_configured(false),
     m_commandedTrigger(nullptr) {
}



QuestNPSender::~QuestNPSender(){
   if (m_commandedTrigger) {
      m_commandedTrigger->_destroy();
      m_commandedTrigger=0;
   }

   if (m_quest.size()>0) {
      unconfigure();
   }
}

void QuestNPSender::configure(const daq::rc::TransitionCmd&){
   Configuration confDB("");

   std::string any;
   const daq::core::Partition* dbPartition=
      daq::core::get_partition(confDB, any);
   if(dbPartition==0) {
      throw(QuestnpsendConfigIssue(ERS_HERE,"Failed to get partition"));
   }

   const daq::df::QuestNPSender* dbApp=
      confDB.get<daq::df::QuestNPSender>(m_name);
   if (dbApp==0) {
      throw(QuestnpsendConfigIssue(ERS_HERE,
                                   "- Application "+m_name+" not found "));
   }

   // Ask that all $variables in the DB are automatically substituted
   confDB.register_converter(
      new daq::core::SubstituteVariables(*dbPartition));
   m_partitionName=dbPartition->UID();
   if (m_ipcPartition==0) {
      m_ipcPartition=new IPCPartition(m_partitionName.c_str());
   }
   m_dictionary=ISInfoDictionary(*m_ipcPartition);
   m_isInfoName=dbApp->get_ISServerName()+".QuestNPSender."+m_name;

   m_replaceL1Id=dbApp->get_replaceL1Id();
   m_startDelay=dbApp->get_startDelay();
   m_delayBetweenEvents=dbApp->get_delayBetweenEvents();

   // Sort out channel list
   // ======================
   std::cout << "QuestNPSender::configure() sorting channel list\n";
   std::cout.flush();
   std::vector<const daq::core::Component*> disabledComponents
      =dbPartition->get_Disabled();

   unsigned int modnum=0;
   for (auto moduleIter=dbApp->get_Contains().begin(); 
        moduleIter != dbApp->get_Contains().end(); moduleIter++) {

      if (!(*moduleIter)->disabled(*dbPartition)) {
         m_quest.emplace_back(new QuestNP());
         m_chanIds.emplace_back(std::vector<unsigned int>());
         m_chanAddr.emplace_back(std::vector<unsigned int>());
         auto module=confDB.cast<daq::df::QuestNPModule>(*moduleIter);
         auto card=module->get_address();
         m_quest[modnum]->card_open(card);

         m_questInfo.emplace_back(QuestNPInfo());
         initStats(modnum);
         std::ostringstream ss;
         ss << dbApp->get_ISServerName() << ".QuestNPSender."<<m_name
            << ".QuestNP"<<modnum;;
         m_questInfoName.emplace_back(ss.str());
      }

      const daq::df::QuestNPModule* module=confDB.cast<daq::df::QuestNPModule,
         daq::core::ResourceBase> (*moduleIter);
      if (module==0) {
         ers::error(
            QuestnpsendConfigIssue(
               ERS_HERE,
               "Questnpsend contains something that is not a ReadoutModule"));
         continue;
      }
      const daq::core::ResourceSet*  modResources=
         confDB.cast<daq::core::ResourceSet, daq::core::ResourceBase> (
            *moduleIter);
      if (modResources==0) {
         ers::error(
            QuestnpsendConfigIssue(
               ERS_HERE,
               "Questnpsend contains something that is not a ResourceSet")
            );
         continue;
      }
      for (std::vector<const daq::core::ResourceBase*>::const_iterator
              channelIter=modResources->get_Contains().begin(); 
           channelIter != modResources->get_Contains().end(); channelIter++) {

         if (!(*channelIter)->disabled(*dbPartition)) {
            auto chan=confDB.cast<daq::df::QuestNPChannel>(*channelIter);
            if (chan==0) {
               ers::error(
                  QuestnpsendConfigIssue(
                     ERS_HERE,
                     "Questnpsend contains something not a QuestNPChannel"));
               continue;
            }
            else {
               m_chanIds[modnum].push_back(chan->get_Id());
               m_chanAddr[modnum].push_back(chan->get_address());
               m_questInfo[modnum].robId[chan->get_address()]=chan->get_Id();
               m_questInfo[modnum].rolEnabled[chan->get_address()]=true;
            }
         }
      }
      m_rolIdList.insert(m_chanIds[modnum].begin(),m_chanIds[modnum].end());
      modnum++;
   }

   auto fileVec=
      confDB.cast<daq::df::DFParameters, daq::core::DataFlowParameters>(
         dbPartition->get_DataFlowParameters())->get_UsesDataFiles();
   for (auto fileIter : fileVec) {
      auto fileName=fileIter->get_FileName();
      struct stat statBuf;
      std::string splitDir=fileName+".per-rob";
      if (stat(splitDir.c_str(),&statBuf) != 0) {
         // Per ROL data files not available
         loadFile(fileName);
      }
      else {
         // OK, we load one file per ROL
         for (auto chanIter : m_rolIdList) {
            std::ostringstream fnStream;
            fnStream << splitDir << "/0x" << std::setfill('0')
                     << std::setw(8) << std::uppercase << std::hex
                     << chanIter << ".data";
            loadFile(fnStream.str());
         }
      }
   }

   auto evIter=m_store.begin();
   m_isInfo.firstEv=evIter->first;
   auto evRIter=m_store.rbegin();
   m_isInfo.lastEv=evRIter->first;
   m_isInfo.nEvents=m_store.size();
   m_isInfo.latestAvailable=m_isInfo.lastEv;

   for (unsigned int modnum=0;modnum<m_quest.size();modnum++) {
      for (unsigned int chan=0;chan<12;chan++) {
         m_quest[modnum]->channel_reset(chan);
//         m_quest[modnum]->slink_reset(chan);
      }
   }

   auto masterTriggerInPartition = dbPartition->get_MasterTrigger();
   if (masterTriggerInPartition != nullptr) {
     ERS_LOG("MasterTrigger UID (from OKS): " << masterTriggerInPartition->UID());
     if (masterTriggerInPartition->get_TriggerModule() != nullptr) {
       if (masterTriggerInPartition->get_TriggerModule()->UID() == m_name) {
         ERS_LOG("This application is registered as MasterTrigger: instantiating CommandedTrigger object");
         ERS_LOG("Creating CommandedTrigger object for application \"" << m_name << "\"");
         m_commandedTrigger = new daq::trigger::CommandedTrigger(*m_ipcPartition, m_name, this);
       }
     }
   }
   m_configured=true;
}



void QuestNPSender::unconfigure(const daq::rc::TransitionCmd&){
   unconfigure();
}



void QuestNPSender::unconfigure(){
   m_configured=false;
   for (auto quest : m_quest) {
      delete quest;
   }

   std::lock_guard<std::mutex> lock(m_statsMutex);
   if (!m_interactive) {
      ISInfoDictionary dictionary(*m_ipcPartition);
      try {
         dictionary.remove(m_isInfoName);
      }
      catch (daq::is::Exception& exc) {
         // Ignore error at this stage.  It may be that we have never
         // published anything since configure or somebody removed the
         // item behind our back.
      }
      for (unsigned int modnum=0;modnum<m_questInfo.size();modnum++) {
         try {
            dictionary.remove(m_questInfoName[modnum]);
         }
         catch (daq::is::Exception& exc) {
            // Ignore error at this stage.  It may be that we have
            // never published anything since configure or somebody
            // removed the item behind our back.
         }
      }
   }
   m_quest.clear();
   m_questInfo.clear();
   m_questInfoName.clear();
   m_chanIds.clear();

   if (m_ipcPartition!=0){
      delete m_ipcPartition;
      m_ipcPartition=0;
   }

   for (auto evIter : m_store) {
      for (auto robIter : evIter.second) {
         delete robIter.second;
      }
   }
   m_store.clear();
   m_storeSize=0;

   m_isInfo.firstEv=0;
   m_isInfo.lastEv=0;
   m_isInfo.nEvents=0;
   m_isInfo.latestAvailable=0;
}



void QuestNPSender::prepareForRun(const daq::rc::TransitionCmd&){
   clearStats();
   m_runActive=true;
   int tnum=0;
   for (unsigned int modnum=0;modnum<m_quest.size();modnum++) {
      auto nchans=m_chanIds[modnum].size();
      for (unsigned int channum=0;channum<nchans;channum++) {
         m_thread.emplace_back(std::thread([this,modnum,channum](){
                  sendLoop(modnum,channum);
            }));
         std::ostringstream ss;
         ss << "send_"<<std::hex<<m_chanIds[modnum][channum];
         pthread_setname_np(m_thread[tnum].native_handle(),ss.str().c_str());
         tnum++;
      }
   }
}



void QuestNPSender::stopDC(const daq::rc::TransitionCmd&){
   m_runActive=false;

   // Allow a little time for threads to terminate of their own accord
   // before we cancel them
   std::this_thread::sleep_for(std::chrono::milliseconds(200));

   for (unsigned int threadNum=0;threadNum<m_thread.size();threadNum++) {
      if (m_thread[threadNum].joinable()) {
         pthread_cancel(m_thread[threadNum].native_handle());
         //std::cout << "Joining thread  " << threadNum << std::endl;
         m_thread[threadNum].join();
      }
   }
   m_thread.clear();
   for (unsigned int modnum=0;modnum<m_quest.size();modnum++) {
      for (unsigned int chan=0;chan<12;chan++) {
         m_quest[modnum]->channel_reset(chan);
      }
   }
}



void QuestNPSender::publish(){
   if (!m_configured) {
      return;
   }
   std::lock_guard<std::mutex> lock(m_statsMutex);
   if (m_interactive) {
      std::cout << m_isInfo << std::endl;
   }
   else {
      try {
         m_dictionary.update(m_isInfoName,m_isInfo);
      }
      catch (daq::is::InfoNotFound& exc1) {
         try {
            m_dictionary.insert(m_isInfoName,m_isInfo);
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
      }
      catch (daq::is::Exception& exc) {
         // Issue a warning and carry on
         ers::warning(exc);
      }
   }
   for (unsigned int modnum=0;modnum<m_questInfo.size();modnum++) {
      T_quest_config cfg;
      m_quest[modnum]->get_config(&cfg);
      for (unsigned int chan=0;chan<12;chan++) {
         if (cfg.channel[chan].channel_number!=chan) {
            ers::warning(QuestnpsendBadReadIssue(
                            ERS_HERE,chan,cfg.channel[chan].channel_number));
         }
         m_questInfo[modnum].fifoFullCount[chan]=
            cfg.channel[chan].fifo_full_counter;
         m_questInfo[modnum].txRatio[chan]=  //250.0/255.0*
            cfg.channel[chan].data_transfer_ratio;

         m_questInfo[modnum].rolDownStat[chan]=!cfg.channel[chan].link_up;
         m_questInfo[modnum].fifoFull[chan]=cfg.channel[chan].fifo_full;
         m_questInfo[modnum].numberOfLdowns[chan]=
            cfg.channel[chan].link_down_count;
         m_questInfo[modnum].rolXoffStat[chan]=cfg.channel[chan].xoff;
      }


      if (m_interactive) {
         std::cout << m_questInfo[modnum] << std::endl;
      }
      else {
         try {
            m_dictionary.update(m_questInfoName[modnum],m_questInfo[modnum]);
         }
         catch (daq::is::InfoNotFound& exc1) {
            try {
               m_dictionary.insert(m_questInfoName[modnum],m_questInfo[modnum]);
            }
            catch (daq::is::Exception& exc) {
               // Issue a warning and carry on
               ers::warning(exc);
            }
         }
         catch (daq::is::Exception& exc) {
            // Issue a warning and carry on
            ers::warning(exc);
         }
      }
   }
}


daq::trigger::HoldTriggerInfo QuestNPSender::hold(const std::string& dm){
  ERS_INFO("Holding trigger");
  m_triggerHeld = true;
  daq::trigger::HoldTriggerInfo hti{0,0}; // We don't have an ECR count or ext L1 id!
  return hti;
}

void QuestNPSender::resume(const std::string& dm){
  ERS_INFO("Resuming trigger");
  m_triggerHeld = false;
}

void QuestNPSender::increaseLumiBlock(uint32_t runno){}
void QuestNPSender::setLumiBlockInterval(uint32_t seconds){}
void QuestNPSender::setMinLumiBlockLength(uint32_t seconds){}


void QuestNPSender::clearStats(){
   std::lock_guard<std::mutex> lock(m_statsMutex);
   for (unsigned int modnum=0;modnum<m_questInfo.size();modnum++) {
      for (unsigned int chan=0;chan<12;chan++) {
         m_questInfo[modnum].eventsSent[chan]=0;
         m_questInfo[modnum].mostRecentId[chan]=0;
      }
   }
}



void QuestNPSender::initStats(unsigned int modnum){
   m_questInfo[modnum].robId.resize(12,0);
   m_questInfo[modnum].eventsSent.resize(12,0);
   m_questInfo[modnum].mostRecentId.resize(12,0);
   m_questInfo[modnum].rolEnabled.resize(12,false);

   m_questInfo[modnum].txRatio.resize(12,0);
   m_questInfo[modnum].numberOfLdowns.resize(12,0);
   m_questInfo[modnum].fifoFullCount.resize(12,0);

   m_questInfo[modnum].rolDownStat.resize(12,false);
   m_questInfo[modnum].rolXoffStat.resize(12,false);
   m_questInfo[modnum].fifoFull.resize(12,false);
}



void QuestNPSender::loadFile(const std::string& fileSpec){
   std::cout << "Loading data from " << fileSpec << std::endl;
   // Check that the input file exists.  FileStorage would assume its
   // fileName argument is an output file  if it doesn't exist i.e. we don't
   // get an error from it.
   struct stat statBuf;
   if (stat(fileSpec.c_str(),&statBuf) != 0) {
      throw(QuestnpsendConfigIssue(ERS_HERE,
                                   "- Data file "+fileSpec+" not found "));
   }

   DataReader* reader=pickDataReader(fileSpec);
   while (reader->good()) {
      unsigned int eventSize;
      char* buffer;
      DRError errorCode = reader->getData(eventSize,&buffer);
      if (errorCode==DROK) {
         //std::cout << "read event size " << eventSize << std::endl;
         unsigned int* header=reinterpret_cast<uint32_t*>(buffer);
         if (header[0]==0xaa1234aa) {
            if (eformat::peek_major_version(header)!=
                (eformat::DEFAULT_VERSION>>16)) {
               unsigned int* newbuf=new unsigned int[eventSize/4];
               eformat::old::convert(
                  header,newbuf,eventSize/4,eformat::CRC16_CCITT);
               delete [] buffer;
               buffer=(char*)newbuf;
               header=newbuf;
            }
            const eformat::FullEventFragment<const uint32_t*> fragment(header);
            //std::cout << "Calling unpackFragments for event " << fragment.lvl1_id() << std::endl;
            unpackFragments(fragment);
         }
         else {
            QuestnpsendBadMarkerIssue ex(ERS_HERE,fileSpec,header[0]);
            ers::error(ex);
         }
      }
      else {
         QuestnpsendBadLoadIssue ex(ERS_HERE,fileSpec,errorCode);
         ers::error(ex);
      }
      delete [] buffer;
   }
}


void QuestNPSender::sendLoop(unsigned int modnum, unsigned int channum) {
   unsigned int l1Id=0xffffffff;
   if (m_startDelay!=0) {
      std::this_thread::sleep_for(std::chrono::milliseconds(m_startDelay));
   }
   while (m_runActive) {
      for (auto event : m_store) {
         auto endIter=event.second.end();
         auto robIter=event.second.find(m_chanIds[modnum][channum]);
         if(m_triggerHeld == false && robIter!=endIter) {
            eformat::ROBFragment<const uint32_t*>
               robFragment(robIter->second);
            if (m_replaceL1Id) {
               l1Id++;
               unsigned int* rodPtr=const_cast<unsigned int*>(
                  robFragment.rod_start());
               rodPtr[5]=l1Id;
            }
            else {
               l1Id=event.first;
            }
            if (m_delayBetweenEvents!=0) {
              std::this_thread::sleep_for(std::chrono::microseconds(m_delayBetweenEvents));
            }
            //std::cout << "Sending fragment with l1 id " << std::hex << event.first << " as " << l1Id << std::dec <<std::endl;
            unsigned int chanAddr=m_chanAddr[modnum][channum];
            m_quest[modnum]->send_packet(chanAddr,
                                         robFragment.rod_fragment_size_word()*4,
                                         robFragment.rod_start());
            m_questInfo[modnum].mostRecentId[chanAddr]=l1Id;
            m_questInfo[modnum].eventsSent[chanAddr]++;
         }
         if (!m_runActive) {
            break;
         }
      }
   }
}

void QuestNPSender::unpackFragments(
   const eformat::FullEventFragment<const uint32_t*>& event) {
   uint32_t level1Id = event.lvl1_id();
   if (level1Id > m_maxLevel1) {
      m_maxLevel1=level1Id;
   }
   auto storeIter=m_store.find(level1Id);
   if (storeIter==m_store.end()) {
      ///Adds a new entry with the new L1 identifier
      m_store[level1Id]=EventMap();
   }

   //std::cout << "Processing l1Id " << std::hex << level1Id;
   for (unsigned int rob=0; rob<event.nchildren(); rob++) {
      const uint32_t* robp;
      event.child(robp, rob);
      eformat::ROBFragment<const uint32_t*> robFragment(robp);
      unsigned int  rolId=robFragment.source_id();

      // Am I interested on this ROB identifier?
      if (m_rolIdList.find(rolId)!= m_rolIdList.end()) {
         //std::cout << " adding ROL " << rolId;
         auto rolIter=m_store[level1Id].find(rolId);
         //if I already have an entry there...
         if (rolIter!=m_store[level1Id].end()) {
            QuestnpsendBadROBIssue ex(
               ERS_HERE,"- Duplicate ROB ID ",rolId, level1Id);
            ers::warning(ex);
            continue;
         }

         //  Now I need to store the data somewhere and keep a
         //  pointer to it in s_store
         uint32_t* storage=new uint32_t[robFragment.fragment_size_word()];
         memcpy(storage, robp, robFragment.fragment_size_word()*4);
         m_store[level1Id][rolId]=storage;
         m_storeSize+=robFragment.fragment_size_word()*4;
         if (robFragment.fragment_size_word()>m_maxFragSize) {
            m_maxFragSize=robFragment.fragment_size_word();
         }
      }
   }
}



int main(int argc, char* argv[]){
   daq::rc::CmdLineParser parser(argc,argv,false);
   std::shared_ptr<QuestNPSender> theApp=
      std::make_shared<QuestNPSender>(parser.applicationName(),
                                      parser.interactiveMode());
   daq::rc::ItemCtrl myItem(parser, theApp); 
   myItem.init();
   myItem.run();

   std::cout << "Finished...\n";
   return 0;
}
