#ifndef QUESTNPSENDISSUES_H
#define QUESTNPSENDISSUES_H

#include "ers/Issue.h"
ERS_DECLARE_ISSUE(questnpsend,
                  QuestnpsendIssue,
                  " Questnpsend Issue "
                  ERS_EMPTY,
                  ERS_EMPTY
                  )

ERS_DECLARE_ISSUE(questnpsend,
                  QuestnpsendConfigIssue,
                  " Configuration error "  << reason
                  ERS_EMPTY,
                  ((std::string)reason)
                  )

ERS_DECLARE_ISSUE(questnpsend,
                  QuestnpsendBadROBIssue,
                  " duplicate ROB Id " << robId << " for event " <<eventId 
                  ERS_EMPTY,
                  ((unsigned int) robId)
                  ((unsigned int) eventId)
                  )
ERS_DECLARE_ISSUE(questnpsend,
                  QuestnpsendBadMarkerIssue,
                  " error loading file " << file << ", bad marker " << std::hex << marker <<std::dec
                  ERS_EMPTY,
                  ((std::string) file)
                  ((unsigned int) marker)
                  )
ERS_DECLARE_ISSUE(questnpsend,
                  QuestnpsendBadLoadIssue,
                  " error loading file " << file << ", getData returned " << code 
                  ERS_EMPTY,
                  ((std::string) file)
                  ((unsigned int) code)
                  )
ERS_DECLARE_ISSUE(questnpsend,
                  QuestnpsendBadReadIssue,
                  " reading status for cnannel " << chanWant << ", got data for channel " << chanGot
                  ERS_EMPTY,
                  ((unsigned int) chanWant)
                  ((unsigned int) chanGot)
                  )

#endif
