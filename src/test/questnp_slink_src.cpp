/************************************************************************/
/*									*/
/* File: questnp_slink_src.cpp						*/
/*									*/
/* Read event fragments from a file 					*/
/* and send them out on one or several single S-Link channels		*/
/*									*/
/*  23. Nov. 18  MAJO  created						*/
/*									*/
/**************** C 2018 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "ROSQuestNP/questnp.h"
#include "ROSQuestNP/QuestNPException.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

#define BUFSIZE  (1024 * 1024)       //bytes

//Globals
u_int dblevel = 0, dbpackage = DFDB_ROSQUEST;
u_long virtbase, pcibase;
int verbose = FALSE;
int questnp_channel = 1;
u_int rol_mask = 1;
int clockfreq = 1;
int readfile = FALSE;
char filename[200] = {0};
QuestNP questnp1, questnp2;


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-r (HEX) 0x: ROL mask  (0x000001 - 0xffffff) each bit is one ROL  -> Default: " << rol_mask << std::endl;
  std::cout << "-c x: Clock frequency (1 = 100 MHz, 2 = 125 MHz)                  -> Default: " << clockfreq << std::endl;
  std::cout << "-f x: Read data from file. The parameter is the path and the name of the file" << std::endl;
  std::cout << "-v  : Verbose output                                              -> Default: FALSE" << std::endl;
  std::cout << "-d  : Debug level                                                 -> Default: 0" << std::endl;
  std::cout << "-D  : Debug package                                               -> Default: 14" << std::endl;
  std::cout << std::endl;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int c, ret, memhandle;
  u_int word_number,  *data;
  u_int fret, event_id = 0, event_size, edloop; 

  static struct option long_options[] = {"DVS", no_argument, NULL, '1'}; 

  while ((c = getopt_long(argc, argv, "r:c:f:vd:D:h", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(-1);
      break;

    case 'f':   
      readfile = TRUE;
      sscanf(optarg,"%s", filename);
      std::cout << "read data from file " << filename << std::endl;
      break;
      
    case 'r': sscanf(optarg, "%x", &rol_mask); break;  
    case 'c': clockfreq = atoi(optarg);       break;
    case 'v': verbose = TRUE;                 break;
    case 'd': dblevel = atoi(optarg);         break;
    case 'D': dbpackage = atoi(optarg);       break;                   

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (-1);
    }
  
  questnp_channel -= 1; //QuestNP channel numbering starts at 0

  // Initialize the debug macro
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);      
    
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  ret = CMEM_BPASegmentAllocate(BUFSIZE, (char *)"QUEST_BUFFER", &memhandle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ret = CMEM_SegmentVirtualAddress(memhandle, &virtbase);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(memhandle, &pcibase);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("Contiguous buffer:\n");
  printf("Physical address = 0x%08lx\n", pcibase);
  printf("Virtual address  = 0x%08lx\n", virtbase);
  data = (u_int *) (virtbase);


  try
  {
    if (rol_mask & 0xfff)
    {
      questnp1.card_open(0);

      if (verbose)
	std::cout << "Calling configure_i2c_clock" << std::endl;

      if (clockfreq == 1)
	questnp1.configure_i2c_clock(100.0);
      else if (clockfreq == 2)
	questnp1.configure_i2c_clock(125.0);
    }
    
    if (rol_mask & 0xfff000)
    {
      questnp2.card_open(1);

      if (verbose)
	std::cout << "Calling configure_i2c_clock" << std::endl;

      if (clockfreq == 1)
	questnp2.configure_i2c_clock(100.0);
      else if (clockfreq == 2)
	questnp2.configure_i2c_clock(125.0);
    }

    for (questnp_channel = 0; questnp_channel < 12; questnp_channel++)
    {
      if (rol_mask & (1 << questnp_channel))
      {
	if (verbose)
	  std::cout << "Resetting S-Link of channel" << questnp_channel << std::endl;

	questnp1.slink_reset(questnp_channel);

	if (verbose)
	  std::cout << "Resetting channel" << questnp_channel << std::endl;

	questnp1.channel_reset(questnp_channel);
      }
    }
    
    for (questnp_channel = 0; questnp_channel < 12; questnp_channel++)
    {
      if ((rol_mask >> 12) & (1 << questnp_channel))
      {
	if (verbose)
	  std::cout << "Resetting S-Link of channel" << questnp_channel << std::endl;

	questnp2.slink_reset(questnp_channel);

	if (verbose)
	  std::cout << "Resetting channel" << questnp_channel << std::endl;

	questnp2.channel_reset(questnp_channel);
      }
    }

    //Open the input file
    FILE *inf;
    inf = fopen(filename, "r");
    if(inf == 0)
    {
      std::cout << "Can't open input file " << filename << std::endl;
      exit(-1);
    }

    word_number = 0;

    while(!feof(inf))
    {
      //Read fragment size
      fret = fscanf(inf, "%x", &event_size);
      if (fret != 1)
	break;
      word_number++;

      if (verbose)
	std::cout << "ROD Event size = " << event_size << " words." << std::endl;

      if (event_size > BUFSIZE)
      {
	std::cout << "Event is too big" << std::endl;
	exit(-1);
      }

      //Read the event data
      for (edloop = 0; edloop < event_size; edloop++)
      {
	fscanf(inf, "%x", &data[edloop]);
	if (verbose && edloop < 5)
	  std::cout << "Data word " << word_number << " is " << HEX(data[edloop]) << std::endl;
	word_number++;
      }

      data[5] = event_id;         //Replace the original event ID


      if (verbose)
	std::cout << "Sending L1ID = " << event_id << std::endl;

      for (questnp_channel = 0; questnp_channel < 12; questnp_channel++)
      {
	if (rol_mask & (1 << questnp_channel))
	{
          questnp1.send_packet(questnp_channel, event_size * 4, data); 
	}
      }

      for (questnp_channel = 0; questnp_channel < 12; questnp_channel++)
      {
	if ((rol_mask >> 12) & (1 << questnp_channel))
	{
          questnp2.send_packet(questnp_channel, event_size * 4, data); 
	}
      }

      event_id++;
    }	
    
    std::cout << "In total the file had " << event_id << " events." << std::endl;
    if (rol_mask & 0xfff)
      questnp1.card_close();
    if (rol_mask & 0xfff000)
      questnp2.card_close();
  }
  catch(QuestNPException &ex)
  {
    std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
    exit(-1);
  }

  ret = CMEM_BPASegmentFree(memhandle);
  if (ret)
    rcc_error_print(stdout, ret);
 
  ret = CMEM_Close();
  if (ret)
    rcc_error_print(stdout, ret);
   
  return(0);
}


