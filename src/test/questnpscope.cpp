/****************************************************************/
/*                                                              */
/*  file: questnpscope.cpp                                      */
/*                                                              */
/* This program allows to access the resources of a C-RORC      */
/* card with the QUESTNP F/W in a user friendly way   		*/
/*                                                              */
/*  Author: Markus Joos, CERN-EP                                */
/*                                                              */
/*  14 June 2018  MAJO  created                                 */
/*                                                              */
/****************C 2018 - A nickel program worth a dime**********/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "io_rcc/io_rcc.h"
#include "ROSGetInput/get_input.h"
#include "ROSQuestNP/questnp.h"
#include "cmem_rcc/cmem_rcc.h"

#define BUFSIZE         0x100000


/*globals*/
static u_int shandle, offset, cont;
static u_long sreg;
static T_questnp_regs *questnp;
static struct sigaction sa, sa2;
u_long virtbase, pcibase;


/**********************/
int questnp_map(int occ)
/**********************/
{
  unsigned int eret, pciaddr;  //MJ: do we need a 64bit PCI address?

  eret = IO_Open();
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }

  eret = IO_PCIDeviceLink(0x10dc, 0x01aa, occ, &shandle);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }  

  eret = IO_PCIConfigReadUInt(shandle, 0x10, &pciaddr);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  //printf("BAR 0 is at 0x%08x\n", pciaddr);
  pciaddr = pciaddr & 0xfffffff0;  //get rid of special bits
  offset = pciaddr & 0xfff;        //4KB alignment
  pciaddr &= 0xfffff000;
  
  eret = IO_PCIMemMap(pciaddr, 0x40000, &sreg);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  //printf("IO_PCIMemMap returns address is at 0x%016lx\n", sreg);
  //printf("offset is 0x%08x\n", offset);

  questnp = (T_questnp_regs *)(sreg + offset); 
  //printf("design_id_l is at address %p\n", (void *)&questnp->design_id_l);
  return(0);
}


/*********************/
int questnp_unmap(void)
/*********************/
{
  unsigned int eret;

  eret = IO_PCIMemUnmap(sreg, 0x1000);
  if (eret)
    rcc_error_print(stdout, eret);

  eret = IO_Close();
  if (eret)
    rcc_error_print(stdout, eret);
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("Contact Markus Joos, 72364, 160663, markus.joos@cern.ch if you need help\n");
  return(0);
}


/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
  cont = 0;
  signum = signum;
  alarm(0);
}


/***************************/
void AlarmHandler(int signum)
/***************************/
{
  signum = signum; // get rid of compiler warning
  printf("Number of data blocks sent: ");
  if (cont) 
    alarm(2);
}    


/*********************/
void check_struct(void)
/*********************/
{
  T_questnp_regs *qnp;

  qnp = (T_questnp_regs *)0;

  printf("design_id_l                 is at offset %p\n", (void *)&qnp->design_id_l);
  printf("design_id_h                 is at offset %p\n", (void *)&qnp->design_id_h);
  printf("gcr                         is at offset %p\n", (void *)&qnp->gcr);
  printf("gsr                         is at offset %p\n", (void *)&qnp->gsr);
  printf("icr                         is at offset %p\n", (void *)&qnp->icr);
  printf("imr                         is at offset %p\n", (void *)&qnp->imr);
  printf("isr                         is at offset %p\n", (void *)&qnp->isr);
  //printf("i2cc                      is at offset %p\n", (void *)&qnp->i2cc);
  printf("i2csc                       is at offset %p\n", (void *)&qnp->i2csc);
  printf("channel[0].cw_port_a[0]     is at offset %p\n", (void *)&qnp->channel[0].cw_port_a[0]);
  printf("channel[0].data_port_a32[0] is at offset %p\n", (void *)&qnp->channel[0].data_port_a32[0]);
  printf("channel[0].cw_port_b[0]     is at offset %p\n", (void *)&qnp->channel[0].cw_port_b[0]);
  printf("channel[0].csr_low          is at offset %p\n", (void *)&qnp->channel[0].csr_low);
  printf("channel[0].csr_high         is at offset %p\n", (void *)&qnp->channel[0].csr_high);
  printf("channel[0].csr_dummy[0]     is at offset %p\n", (void *)&qnp->channel[0].csr_dummy[0]);
  printf("channel[0].dma[0]           is at offset %p\n", (void *)&qnp->channel[0].dma[0]);
  printf("channel[1].cw_port_a[0]     is at offset %p\n", (void *)&qnp->channel[1].cw_port_a[0]);
  printf("channel[1].data_port_a32[0] is at offset %p\n", (void *)&qnp->channel[1].data_port_a32[0]);
  printf("channel[1].cw_port_b[0]     is at offset %p\n", (void *)&qnp->channel[1].cw_port_b[0]);
  printf("channel[1].csr_low          is at offset %p\n", (void *)&qnp->channel[1].csr_low);
  printf("channel[1].dma[0]           is at offset %p\n", (void *)&qnp->channel[1].dma[0]);
}


/****************/
void dumpreg(void)
/****************/
{
  u_long ldata;
  u_int loop;

  ldata = questnp->design_id_l;
  printf("Decoding the Design ID (0x%016lx)\n", ldata);
  printf("   Minor revision                  = 0x%x\n", (int)ldata & 0xffff);  
  printf("   Major revision                  = 0x%x\n", (int)(ldata >> 16) & 0xff);  
  printf("   Version                         = 0x%x\n", (int)(ldata >> 24) & 0xff);  
  printf("   Number of channels per SubQuest = %d\n", (int)(ldata >> 40) & 0xff);  
  printf("   Number of SubQuests             = %d\n", (int)(ldata >> 48) & 0xff);  
  printf("   Number of channels              = %d\n", (int)(ldata >> 56) & 0xff);  
  
  for(loop = 0; loop < 12; loop++)
  {
    ldata = questnp->channel[loop].csr_high;
    printf("\nDecoding the Channel Status Register of channel %d (0x%016lx)\n", loop, ldata);
    printf("   S-Link FIFO full counter = %d\n", (int)ldata & 0xff);
    printf("   Data Transfer Ratio      = %d\n", (int)(ldata >> 8) & 0xff);
    printf("   S-Link Link up           = %s\n", ((ldata >> 16) & 1)?"Yes":"No");
    printf("   S-Link FIFO Full         = %s\n", ((ldata >> 17) & 1)?"Yes":"No");
    printf("   RX in Sync               = %s\n", ((ldata >> 27) & 0x1)?"Yes":"No");  
    printf("   GTX reset done           = %s\n", ((ldata >> 28) & 0x1)?"Yes":"No");  
    printf("   RX PLL locked            = %s\n", ((ldata >> 29) & 0x1)?"Yes":"No");  
    printf("   TX FIFO Fill             = %d\n", (int)(ldata >> 32) & 0x3f);  
    printf("   Channel number           = %d\n", (int)(ldata >> 39) & 0x1f);  

    ldata = questnp->channel[loop].csr_low;
    printf("\nDecoding the Channel Control Register of channel %d (0x%016lx)\n", loop, ldata);
    printf("   Zero transition counter  = %s\n", ((int)ldata & 0x1)?"Set":"Not set");
    printf("   Zero link down counter   = %s\n", ((int)(ldata >> 1) & 0x1)?"Set":"Not set");
    printf("   UReset                   = %s\n", ((int)(ldata >> 2) & 0x1)?"Set":"Not set");
    printf("   HOLA reset               = %s\n", ((int)(ldata >> 3) & 0x1)?"Set":"Not set");
    printf("   GTX reset                = %s\n", ((int)(ldata >> 4) & 0x1)?"Set":"Not set");
    printf("   Channel reset            = %s\n", ((int)(ldata >> 5) & 0x1)?"Set":"Not set");
    printf("   Enable unpacker          = %s\n", ((int)(ldata >> 6) & 0x1)?"Set":"Not set");
  }
}


/***************/
void piodma(void)
/***************/
{
  u_long csr_data;
  
  printf("v2\n");

  csr_data = questnp->channel[0].csr_high;
  printf("csr_data_high = 0x%016lx\n", csr_data);
  
  csr_data = questnp->channel[0].csr_low;
  printf("csr_data_low = 0x%016lx\n", csr_data);
  csr_data = csr_data | 0x40;      //setting the "Enable Unpacker" bit
  //csr_data = csr_data & ~0x40;      //remove the "Enable Unpacker" bit
  questnp->channel[0].csr_low = csr_data;

  questnp->channel[0].data_port_a32[0] = 0x00120001;   //BOF in word 1, EOF in word 18
  questnp->channel[0].data_port_a32[1] = 0;            //empty half of header
  questnp->channel[0].data_port_a32[2] = 0xB0F00000;
  questnp->channel[0].data_port_a32[3] = 0xee1234ee;   //header marker
  questnp->channel[0].data_port_a32[4] = 0x00000009;   //header size
  questnp->channel[0].data_port_a32[5] = 0x03010000;   //format version
  questnp->channel[0].data_port_a32[6] = 0x00a00000;   //source ID
  questnp->channel[0].data_port_a32[7] = 0x00000001;   //run number  
  questnp->channel[0].data_port_a32[8] = 0x00000000;   //L1ID  
  questnp->channel[0].data_port_a32[9] = 0x00000000;   //BCID
  questnp->channel[0].data_port_a32[10] = 0x00000007;   //trigger type
  questnp->channel[0].data_port_a32[11] = 0x0000000a;  //detector event type
  questnp->channel[0].data_port_a32[12] = 0x00000000;  //status word
  questnp->channel[0].data_port_a32[13] = 0xdddd0000;  //user data 1
  questnp->channel[0].data_port_a32[14] = 0xdddd0001;  //user data 2
  questnp->channel[0].data_port_a32[15] = 0xdddd0002;  //user data 3
  questnp->channel[0].data_port_a32[16] = 0x1;         //# status words
  questnp->channel[0].data_port_a32[17] = 0x3;         //# data words 
  questnp->channel[0].data_port_a32[18] = 0x0;         //status position
  questnp->channel[0].data_port_a32[19] = 0xE0F00000;

  csr_data = questnp->channel[0].csr_high;
  printf("csr_data_high = 0x%016lx\n", csr_data);
}

/***************/
void piodma2(void)
/***************/
{
  u_int loop, *dmaptr;
 
  u_int can_write;
  u_long csr_data, num_in_fifo;
  
  dmaptr = (u_int *)virtbase;
  
  dmaptr[0] = 0x00100001;   //BOF in word 1, /EOF in word 16
  dmaptr[1] = 0;            //EOF in word 16
  dmaptr[2] = 0xB0F00000;
  dmaptr[3] = 0xee1234ee;   //header marker
  dmaptr[4] = 0x00000009;   //header size
  dmaptr[5] = 0x03010000;   //format version
  dmaptr[6] = 0x00a00000;   //source ID
  dmaptr[7] = 0x00000001;   //run number
  dmaptr[8] = 0x00000000;   //BCID
  dmaptr[9] = 0x00000007;   //trigger type
  dmaptr[10] = 0x0000000a;  //detector event type
  dmaptr[11] = 0x00000000;  //status word
  dmaptr[12] = 0xdddd0000;  //user data 1
  dmaptr[13] = 0xdddd0001;  //user data 2
  dmaptr[14] = 0xdddd0002;  //user data 3
  dmaptr[15] = 0x1;         //# status words
  dmaptr[16] = 0x3;         //# data words 
  dmaptr[17] = 0x0;         //status position
  dmaptr[18] = 0xE0F00000;

  csr_data = questnp->channel[0].csr_high;
  printf("csr_data_high = 0x%016lx\n", csr_data);

  csr_data = questnp->channel[0].csr_low;
  printf("csr_data_low = 0x%016lx\n", csr_data);
  csr_data = csr_data | 0x40;      //setting the "Enable Unpacker" bit
  questnp->channel[0].csr_low = csr_data;

  for (loop = 0; loop < 19; loop++)
  {
    can_write = 0;
    while (!can_write)
    {
      csr_data = questnp->channel[0].csr_high;
      num_in_fifo = (csr_data >> 32) & MASK_TX_FIFO_FILL;
      can_write = MAX_TX_FIFO_FILL - num_in_fifo;
      if(can_write)
      {
        printf("Can write %d\n", can_write);
        break;
      }
    }
  
    questnp->channel[0].data_port_a32[loop] = dmaptr[loop];
  }
  
  csr_data = questnp->channel[0].csr_high;
  printf("csr_data_high = 0x%016lx\n", csr_data);
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  static int ret, fun = 1, occ = 1;
  int memhandle;

  if ((argc == 2) && (sscanf(argv[1], "%d", &occ) == 1)) {argc--;} else {occ = 1;}
  if (argc != 1)
  {
    printf("This is QUESTSNPCOPE.\n\n");
    printf("Usage: questscope [QUESTNP occurrence] Note: occurences are enumerated 1..N\n");
    exit(0);
  }

  if (occ == 0)
  {
    printf("Usage: questscope [QUESTNP occurrence] Note: occurences are enumerated 1..N\n");
    exit(0);
  }
  
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  ret = CMEM_BPASegmentAllocate(BUFSIZE, (char *)"QUESTNP_BUFFER", &memhandle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ret = CMEM_SegmentVirtualAddress(memhandle, &virtbase);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(memhandle, &pcibase);
  if (ret)
    rcc_error_print(stdout, ret);
      
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;
  ret = sigaction(SIGQUIT, &sa, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = AlarmHandler;
  ret = sigaction(SIGALRM, &sa2, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  questnp_map(occ);

  printf("\n\n\nThis is QUESTNPSCOPE\n");
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Print help\n");
    printf("  2 Check structure\n");
    printf("  3 Decode registers\n");
    printf("  4 PIO DMA test\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) check_struct();
    if (fun == 3) dumpreg();
    if (fun == 4) piodma();
  }

  questnp_unmap();  
  
  ret = CMEM_BPASegmentFree(memhandle);
  if (ret)
    rcc_error_print(stdout, ret);
 
  ret = CMEM_Close();
  if (ret)
    rcc_error_print(stdout, ret);
}




