/************************************************************************/
/*									*/
/* File: QuestNP_test.c							*/
/*									*/
/* This is the test program for the ROSQuestNP package 			*/
/*									*/
/*  29. Jun. 18  MAJO  created						*/
/*									*/
/**************** C 2018 - A nickel program worth a dime ****************/


#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "ROSQuestNP/questnp.h"
#include "ROSQuestNP/QuestNPException.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"


//Constants 
#define BUFSIZE         0x100000
#define MAXQUESTNPROLS  12

//Prototypes
int func_menu(void);
int mainhelp(void);
void setdebug(void);
int error_menu(void);
void generate_fragment_2(u_long ptr, u_int *size);
void generate_fragment_1(u_long ptr, u_int l1id, u_int size);
void generate_fragment_3(u_long ptr, u_int l1id, u_int *size);
void generate_fragment_4(u_long ptr, u_int l1id, u_int mode, u_int *size);

//Globals
u_int cont;
u_long virtbase, pcibase;
QuestNP questnp_card;


/*********************************/
void SigQuitHandler(int /*signum*/)
/*********************************/
{
  cont = 0;  
  printf("TERMINATED\n");
}


/************/
int main(void)
/************/
{
  int fun = 2;
  int ret;
  int memhandle;
  u_int loop, *ptr;
  struct sigaction sa;
 
  printf("\n\n\nThis is the test program for the QuestNP library\n");
  printf("================================================\n");

  //DF::GlobalDebugSettings::setup(20, DFDB_ROSQUEST);
  
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler; 
  ret = sigaction(SIGQUIT, &sa, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(-1);
  }
  
   
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  ret = CMEM_BPASegmentAllocate(BUFSIZE, (char *)"QUESTNP_BUFFER", &memhandle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ret = CMEM_SegmentVirtualAddress(memhandle, &virtbase);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(memhandle, &pcibase);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("Contiguous buffer:\n");
  printf("Physical address = 0x%08lx\n", pcibase);
  printf("Virtual address  = 0x%08lx\n", virtbase);

  // Initialize the buffer
  ptr = (u_int *) virtbase;
  for(loop = 0; loop < (BUFSIZE >> 2); loop++)
    *ptr++ = loop;

  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                         2 Test functions\n");
    printf("   3 Set debug parameters         4 Send ROD fragments with errors\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) func_menu();
    if (fun == 3) setdebug();
    if (fun == 4) error_menu();
  }

  ret = CMEM_BPASegmentFree(memhandle);
  if (ret)
    rcc_error_print(stdout, ret);
 
  ret = CMEM_Close();
  if (ret)
    rcc_error_print(stdout, ret);
   
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
void setdebug(void)
/*****************/
{
  static unsigned int dblevel = 20, dbpackage = DFDB_ROSQUEST;
  
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
}


/*****************/
int func_menu(void)
/*****************/
{
  static u_int card_opened = 0, loop, card = 0, *data_ptr, size = 16, channel, clockfreq = 1, usernpackets = 0, runforever = 0;
  u_int npackets, packets_sent, l1id, fun = 1;
  T_qsfp_monitoring_data qsfp_data[4];
  T_quest_config quest_config;

  printf("\n=========================================================================\n");
  
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:\n");
    printf("   1 card_open              2 card_close   \n");
    printf("   3 get_QSFP_data          4 send_packet  \n");
    printf("   5 configure_i2c_clock    6 get_config   \n");
    printf("   7 slink_reset            8 channel_reset\n");    
    printf("   9 send_packet_memcpy                    \n");    
    printf("   0 Exit                                  \n");    
    printf("Your choice ");
    fun = getdecd(fun); 

    try
    {
      if (fun == 1)
      {
	printf("Enter the number of the QuestNP card to open [0..N-1]: ");
	card = getdecd(card);      
	questnp_card.card_open(card);
	card_opened = 1;
      }

      if (fun == 2) 
      {
        if(card_opened == 0)
	  printf("WARNING. you have not yet executed card_open\n");
	else
	{
 	  questnp_card.card_close();
	  card_opened = 0;
	}
      }    

      if (fun == 3)
      {
        if(card_opened == 0)
	  printf("WARNING. you have not yet executed card_open\n");
	else
	{
	  for(loop = 0; loop < 3; loop++)
	    questnp_card.get_QSFP_data(loop, &qsfp_data[loop]);

	  printf("QSFP | Vendor           | Part             | Revision | Serial           |     Date   | Temp. | Voltage\n");
	  for(loop = 0; loop < 3; loop++)
	  {
            printf("   %d | ", loop);
            printf("%s | ", qsfp_data[loop].vendor_name);
            printf("%s | ", qsfp_data[loop].part_number);
            printf("      %s | ", qsfp_data[loop].revision);
            printf("%s | ", qsfp_data[loop].serial_number);
            printf("%s | ", qsfp_data[loop].manufacture_date);
            printf(" %d C | ", qsfp_data[loop].temperature);
            printf("%f V\n", qsfp_data[loop].voltage);
	  }    

          printf("\nQSFP | Channel | Rx Power |   Tx Bias | Tx error | Tx loss | Rx loss\n");
	  for(loop = 0; loop < 3; loop++)
	  {
	    for(channel = 0; channel < 4; channel++)
	    {
              printf("   %d |       %d |", loop, channel);
              printf("%6.3f mW | ", qsfp_data[loop].rx_power[channel]);
              printf("%6.3f mA | ", qsfp_data[loop].tx_bias[channel]);
              printf("       %d | ", qsfp_data[loop].tx_error[channel]);
              printf("      %d | ", qsfp_data[loop].loss_of_tx_signal[channel]);
              printf("      %d\n", qsfp_data[loop].loss_of_rx_signal[channel]);
	    }
	  }
	}
      }

      if (fun == 4 || fun == 9) 
      {
        if(card_opened == 0)
	  printf("WARNING. you have not yet executed card_open\n");
	else
	{
	  printf("Enter the channel [0..N-1]: ");
	  channel = getdecd(channel);      
	  printf("Enter the data size in bytes (ROD header and trailer will be added automatically): ");
	  size = getdecd(size);      
	  printf("Enter the number of packets to send (0 = run until <ctrl+\\> pressed): ");
	  usernpackets = getdecd(usernpackets); 
          npackets = usernpackets;

	  data_ptr = (u_int *)virtbase;

	  //Initialize the data	  
	  data_ptr[0] = 0xee1234ee; //header marker
	  data_ptr[1] = 0x00000009; //header size
	  data_ptr[2] = 0x03010000; //format version
	  data_ptr[3] = 0x00a00000; //source ID
	  data_ptr[4] = 0x00000001; //run number
	  data_ptr[6] = 0x00000000; //BCID
	  data_ptr[7] = 0x00000007; //trigger type
	  data_ptr[8] = 0x0000000a; //detector event type
	  data_ptr[9] = 0x00000000; //status word
	  
	  for (loop = 0; loop < (size / 4); loop++)
            data_ptr[10 + loop] = 0xdddd0000 + loop;

	  data_ptr[10 + (size / 4)] = 0x1;
	  data_ptr[11 + (size / 4)] = size / 4;
	  data_ptr[12 + (size / 4)] = 0x0;

	  cont = 1;
          if(npackets == 0)
	  {
	    runforever = 1;
	    printf("=====================================\n");
	    printf("Test running. Press <ctrl+\\> to stop\n");
	    printf("=====================================\n");
	  }
	  else
	    runforever = 0;
          
	  packets_sent = 0;
	  l1id = 0;
	  while(npackets || runforever)
	  {
  	    data_ptr[5] = l1id++;     //L1ID
	    
  	    if (fun == 4)
	      questnp_card.send_packet(channel, size + 52, data_ptr);
	    else
	      questnp_card.send_packet_memcpy(channel, size + 52, data_ptr);
	    
	    npackets--;
	    packets_sent++;
	    
	    if ((packets_sent % 10000) == 0)
	    {
              questnp_card.get_config(&quest_config);
              printf("%d packets sent. Performance = %6.2f MB/s \n", packets_sent, quest_config.channel[channel].data_transfer_ratio * 250 / 255.0);
	    }
	    
	    if (cont == 0)
	      break;
	  }
        }
      }    
      
      if (fun == 5) 
      {
        if(card_opened == 0)
	  printf("WARNING. you have not yet executed card_open\n");
	else
	{	
	  printf("Select the clock frequency [1 = 100, 2 = 125 MHz]: ");
	  clockfreq = getdecd(clockfreq);      
          if (clockfreq == 1)
            questnp_card.configure_i2c_clock(100.0);
          else if (clockfreq == 2)
            questnp_card.configure_i2c_clock(125.0);
          else
            printf("I said 1 or 2\n");
	}
      }
      
      if (fun == 6) 
      {
        if(card_opened == 0)
	  printf("WARNING. you have not yet executed card_open\n");
	else
	{
          questnp_card.get_config(&quest_config);
	  printf("Decoding the Design ID\n");
	  printf("   Minor revision                  = 0x%x\n", quest_config.minor);  
	  printf("   Major revision                  = 0x%x\n", quest_config.major);  
	  printf("   Version                         = 0x%x\n", quest_config.version);  
	  printf("   Number of channels per SubQuest = %d\n", quest_config.ncps);  
	  printf("   Number of SubQuests             = %d\n", quest_config.nsubquests);  
	  printf("   Number of channels              = %d\n", quest_config.nchanels);  


	  printf("Channel | S-Link FIFO full # | Data Tr. Ratio | S-Link up | RX in Sync | GTX reset done | RX PLL locked | S-Link FIFO Full | TX FIFO Fill | S-Link down # | XOFF |       raw csr_high\n");  

	  for(loop = 0; loop < 12; loop++)
	  {
	    printf("     %2d |", quest_config.channel[loop].channel_number);  
	    printf("               %4d |", quest_config.channel[loop].fifo_full_counter);
	    printf("           %4d |", quest_config.channel[loop].data_transfer_ratio);
	    printf("       %3s |", quest_config.channel[loop].link_up?"Yes":"No");
	    printf("        %3s |", quest_config.channel[loop].rx_sync?"Yes":"No");
   	    printf("            %3s |", quest_config.channel[loop].gtx_reset_done?"Yes":"No");
	    printf("           %3s |", quest_config.channel[loop].rx_pll_locked?"Yes":"No");
	    printf("              %3s |", quest_config.channel[loop].fifo_full?"Yes":"No");
	    printf("         %4d |", quest_config.channel[loop].tx_fifo_fill);  
	    printf("           %3d |", quest_config.channel[loop].link_down_count);  
	    printf("  %3s |", quest_config.channel[loop].xoff?"Yes":"No");
	    printf(" 0x%016lx\n", quest_config.channel[loop].rawdata);  
	  }
        }
      }
     
      if (fun == 7) 
      {
        if(card_opened == 0)
	  printf("WARNING. you have not yet executed card_open\n");
	else
	{
      	  printf("Enter the channel [0..N-1 or 12 for all channels]: ");
	  channel = getdecd(channel);      

       	  if (channel < 12)
	  {
	    try
	    {
              printf("Resetting S-Link of channel %d\n", channel);
	      questnp_card.slink_reset(channel);
	    }
	    catch(QuestNPException &ex)
	    {
	      std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
	    }	  
	  }
	  else
	  {
	    for(loop = 0; loop < 12; loop++)
	    {
	      try
	      {
        	printf("Resetting S-Link of channel %d\n", loop);
		questnp_card.slink_reset(loop);
	      }
	      catch(QuestNPException &ex)
	      {
		std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
	      }	  
	    }  
	  }
        }
      }
      
      if (fun == 8) 
      {
        if(card_opened == 0)
	  printf("WARNING. you have not yet executed card_open\n");
	else
	{
      	  printf("Enter the channel [0..N-1 or 12 for all channels]: ");
	  channel = getdecd(channel);      

       	  if (channel < 12)
	  {
	    try
	    {
              printf("Resetting channel %d\n", channel);
	      questnp_card.channel_reset(channel);
	    }
	    catch(QuestNPException &ex)
	    {
	      std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
	    }	  
	  }
	  else
	  {
	    for(loop = 0; loop < 12; loop++)
	    {
	      try
	      {
        	printf("Resetting channel %d\n", loop);
		questnp_card.channel_reset(loop);
	      }
	      catch(QuestNPException &ex)
	      {
		std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
	      }	  
	    }  
	  }
        }
      }      
    }
    catch(QuestNPException &ex)
    {
      std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
      exit(-1);
    }
  }
  printf("=========================================================================\n\n");
  return(0);
}


/**********************************************************/
void generate_fragment_1(u_long ptr, u_int l1id, u_int size)
/**********************************************************/
{ 
  u_int *data, word = 0, loop, ndatawords;
  
  if(size < 13)
  {
    printf("Error in function generate_fragment_1. Parameter size is %d\n", size);
    exit(-1);
  }

  data = (u_int *) ptr;
  ndatawords = size - 13;    //header + trailer + status
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id;       //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000077; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  for (loop = 0; loop < ndatawords; loop++)
    data[word++] = loop;     //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = ndatawords; //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
}


/***********************************************/
void generate_fragment_2(u_long ptr, u_int *size)
/***********************************************/
{ 
  u_int *data, word = 0;

  data = (u_int *) ptr;
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  *size = word;
}


/***********************************************************/
void generate_fragment_3(u_long ptr, u_int l1id, u_int *size)
/***********************************************************/
{ 
  u_int *data, word = 0;

  data = (u_int *) ptr;
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id;       //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  *size = word;
}


/***********************************************************************/
void generate_fragment_4(u_long ptr, u_int l1id, u_int mode, u_int *size)
/***********************************************************************/
{ 
  u_int *data, word = 0;

  data = (u_int *) ptr;
  if (mode == 0)
    data[word++] = 0xee1111ee; //header marker
  else
    data[word++] = 0xee1234ee; //header marker

  data[word++] = 0x00000009;   //header size
  
  if (mode == 1)
    data[word++] = 0x03000000; //format version
  else
    data[word++] = 0x03010000; //format version

  data[word++] = 0x00000011;   //source ID
  data[word++] = 0x00000022;   //run number 
  data[word++] = l1id;         //L1ID
  data[word++] = 0x00000033;   //BCID
  data[word++] = 0x00000044;   //trigger type
  data[word++] = 0x00000055;   //detector event type

  if (mode == 3)  
    data[word++] = 0x10001000; //status word
  else
    data[word++] = 0x00000000; //status word

  data[word++] = 0x00000000;   //data word
  data[word++] = 0x00000001;   //data word
  data[word++] = 0x00000002;   //data word
  data[word++] = 0x00000003;   //data word
  data[word++] = 0x00000004;   //data word
  data[word++] = 0x00000005;   //data word
  data[word++] = 0x00000006;   //data word
  data[word++] = 0x00000007;   //data word
  data[word++] = 0x00000008;   //data word
  data[word++] = 0x00000009;   //data word
  data[word++] = 0x00000001;   //number of status elements
  
  if (mode == 2)  
    data[word++] = 0x0000000b; //number of data elements
  else
    data[word++] = 0x0000000a; //number of data elements

  data[word++] = 0x00000000;   //status block position (before data)
  *size = word;
}


/******************/
int error_menu(void)
/******************/
{
  int channel, card = 0, clockfreq = 1;
  static u_int nchannels = 1, actchan, size;
  u_int incl1id = 1, fun = 11, l1id = 0, loop;
  u_int isactive[MAXQUESTNPROLS];
  QuestNP questnp;

  printf("\n=========================================================================\n");

  try
  {
    printf("Enter the number of the QuestNP card to open [0..N-1]: ");
    card = getdecd(card);   
    questnp.card_open(card);
   
    printf("Select the clock frequency [1 = 100, 2 = 125 MHz]: ");
    clockfreq = getdecd(clockfreq);      
    if (clockfreq == 1)
      questnp.configure_i2c_clock(100.0);
    else if (clockfreq == 2)
      questnp.configure_i2c_clock(125.0);
    for (loop = 0; loop < MAXQUESTNPROLS; loop++)
      isactive[loop] = 0;

    printf("Enter the number of channels: ");
    nchannels = getdecd(nchannels);

    actchan = 0;
    for(loop = 0; loop < nchannels; loop++)
    {
      printf("Enter the number of channel %d (0..%d): ", loop, MAXQUESTNPROLS - 1);
      actchan = getdecd(actchan);
      isactive[actchan] = 1;

      questnp.slink_reset(actchan);
      questnp.channel_reset(actchan);

      actchan++;
    }

    while (1)  
    {
      printf("\n");
      printf("Select a test:\n");
      printf("   1 Fragment without error or oversized fragment\n");
      printf("   2 Out of sequence fragment\n");
      printf("   3 No data between SCW and ECW\n");
      printf("   4 Data but no L1ID between SCW and ECW\n");
      printf("   5 L1ID but no complete ROD fragment between SCW and ECW\n");
      printf("   6 Incorrect header marker\n");
      printf("   7 Incorrect event format\n");
      printf("   8 Size mismatch\n");
      printf("   9 Non zero status word\n");
      printf("  10 Duplicated fragment\n");
      printf("  11 Fragment with ECR (not an error actually)\n");
      printf("   0 Exit\n");
      printf("Your choice ");
      fun = getdecd(fun); 

      if (fun == 0)  
      {
        questnp.card_close();

	printf("=========================================================================\n\n");
	return(0);
      }

      if (fun == 1)  
      {
	printf("Enter the size of the fragment in words (min. 13)\n");
	size = getdecd(20);
	generate_fragment_1(virtbase, l1id, size);
	incl1id = 1;
      }

      if (fun == 2)
      {  
	l1id += 10;
	size = 64;
	generate_fragment_1(virtbase, l1id, size);
	incl1id = 0;
      }

      if (fun == 3)
      {  
	size = 0;
	incl1id = 1;
      }  

      if (fun == 4)  
      {
	generate_fragment_2(virtbase, &size);
	incl1id = 1;
      }  

      if (fun == 5)  
      {
	generate_fragment_3(virtbase, l1id, &size);
	incl1id = 1;
      }

      if (fun == 6)  
      {
	generate_fragment_4(virtbase, l1id, 0, &size);
	incl1id = 1;
      }

      if (fun == 7)  
      {
	generate_fragment_4(virtbase, l1id, 1, &size);
	incl1id = 1;
      }

      if (fun == 8)  
      {
	generate_fragment_4(virtbase, l1id, 2, &size);
	incl1id = 1;
      }

      if (fun == 9)  
      {
	generate_fragment_4(virtbase, l1id, 3, &size);
	incl1id = 1;
      }

      if (fun == 10)  
      {
	generate_fragment_1(virtbase, l1id - 1, 20);
	incl1id = 1;
      }

      if (fun == 11)
      {  
	l1id = (l1id & 0xff000000) + 0x01000000;
	printf("Enter the size of the fragment in words (min. 13)\n");
	size = getdecd(20);
	generate_fragment_1(virtbase, l1id, size);
	incl1id = 0;
      }

      for (channel = 0; channel < MAXQUESTNPROLS; channel++)
      {
        if (isactive[channel])
          questnp.send_packet(channel, size * 4, (u_int *)virtbase); 

      }
      printf("The fragment has been sent. L1ID = %d\n", l1id);
      if (incl1id)
	l1id++;
    }
  }
  catch(QuestNPException &ex)
  {
    std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
    exit(-1);
  }
}




