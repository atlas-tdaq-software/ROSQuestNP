/************************************************************************/
/*									*/
/*  This is the QUESTNP library 	   				*/
/*  Its supports the QUESTNP F/W on the C_RORC PCIe card    	        */
/*									*/
/*   29. Jun. 18  MAJO  created						*/
/*									*/
/*******C 2018 - The software with that certain something****************/


#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <math.h>
#include <pthread.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "io_rcc/io_rcc.h"
#include "ROSQuestNP/questnp.h"
#include "ROSQuestNP/QuestNPException.h"

//Globals
u_int m_timeout;
static struct sigaction m_sa2;


/***************************/
void AlarmHandler(int signum)
/***************************/
{
  m_timeout = 1;
  //printf("ALARMMMMMM\n");
}


/****************/
QuestNP::QuestNP()
/****************/
{
}


/******************************/
void QuestNP::card_open(int occ)
/******************************/
{  
  u_int ret, shandle, offset, pciaddr;  //MJ: do we need a 64bit PCI address?

  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::card_open: function called"); 

  ret = IO_Open();
  if (ret != IO_RCC_SUCCESS)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::card_open: Failed to open io_rcc package");
    THROW_QUESTNP_EXCEPTION(IORCC, "Failed to open io_rcc package")
  }
  
  ret = IO_PCIDeviceLink(0x10dc, 0x01aa, occ + 1, &shandle);
  if (ret != IO_RCC_SUCCESS)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::card_open: ERROR from IO_PCIDeviceLink");
    THROW_QUESTNP_EXCEPTION(IORCC, "ERROR from IO_PCIDeviceLink")
  }  

  ret = IO_PCIConfigReadUInt(shandle, 0x10, &pciaddr);
  if (ret != IO_RCC_SUCCESS)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::card_open: ERROR from IO_PCIConfigReadUInt");
    THROW_QUESTNP_EXCEPTION(IORCC, "ERROR from IO_PCIConfigReadUInt")
  } 

  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::card_open: BAR 0 is at 0x" << HEX(pciaddr));
  pciaddr = pciaddr & 0xfffffff0;  //get rid of special bits
  offset = pciaddr & 0xfff;        //4KB alignment
  pciaddr &= 0xfffff000;

  ret = IO_PCIMemMap(pciaddr, 0x40000, &m_sreg);
  if (ret != IO_RCC_SUCCESS)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::card_open: ERROR from IO_PCIMemMap");
    THROW_QUESTNP_EXCEPTION(IORCC, "ERROR from IO_PCIMemMap")
  } 

  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::card_open: IO_PCIMemMap returns address 0x" << HEX(m_sreg));

  m_questnp = (T_questnp_regs *)(m_sreg + offset);   
  
  sigemptyset(&m_sa2.sa_mask);
  m_sa2.sa_flags = 0;
  m_sa2.sa_handler = AlarmHandler;
  ret = sigaction(SIGALRM, &m_sa2, NULL);
  if (ret < 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::card_open: failed to install alarm handler"); 
    THROW_QUESTNP_EXCEPTION(ALARM, "ERROR from sigaction")
  }
  
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::card_open: function done"); 
}


/****************************/
void QuestNP::card_close(void)
/****************************/
{
  u_int ret;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::card_close: function called"); 
    
  ret = IO_PCIMemUnmap(m_sreg, 0x40000);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::card_close: ERROR from IO_PCIConfigReadUInt");
    THROW_QUESTNP_EXCEPTION(IORCC, "ERROR from IO_PCIMemUnmap")
  }

  ret = IO_Close();
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::card_close: ERROR from IO_PCIConfigReadUInt");
    THROW_QUESTNP_EXCEPTION(IORCC, "ERROR from IO_Close")
  }
  
  sigemptyset(&m_sa2.sa_mask);
  m_sa2.sa_flags = 0;
  m_sa2.sa_handler = SIG_DFL;
  ret = sigaction(SIGALRM, &m_sa2, NULL);
  if (ret < 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::card_close: failed to remove alarm handler"); 
    THROW_QUESTNP_EXCEPTION(ALARM, "ERROR from sigaction")
  }
  
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::card_close: function done"); 
}


//MJ What are the requirements for the "size": 8, 32 or 64 bit aligned?
/***********************************************************************/
void QuestNP::send_packet(u_int channel, u_int size, const u_int *source)
/***********************************************************************/
{
  u_int loop, size_left, can_write, will_write, data_index, debug_cnt, next_index;
  u_long csr_data, num_in_fifo;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet: Function called with channel = " <<  channel << ", size = " << size << ", source = 0x" << HEX((u_long)source));

  if (size & 0x3)  //size has to be a multiple of 4 bytes
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::send_packet: Size is not a multiple of 4 bytes");
    THROW_QUESTNP_EXCEPTION(SIZE_ERROR, "Size is not a multiple of 4 bytes")
  }

  csr_data = m_questnp->channel[channel].csr_low;
  csr_data = csr_data | 0x40;      //setting the "Enable Unpacker" bit
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet: Writing 0x" << HEX(csr_data) << " to csr_low of channel " << channel);
  m_questnp->channel[channel].csr_low = csr_data;

  m_questnp->channel[channel].cw_port_a[0] = 0xb0f00000;
  
  size_left = size / 4;
  data_index = 0;
  debug_cnt = 0;

  //if the packet size is an odd number of words we first send one 32-bit word
  if(size_left & 0x1)
  {
    m_questnp->channel[channel].data_port_a32[0] = source[data_index];
    data_index++;
    size_left--;
    DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::send_packet: Odd word sent");
  }

  //Now send the rest as 64bit words
  while (size_left)
  {
    if(size_left & 0x1)
    {
      printf("This must never happen\n");
      exit(0);
    }
    
    csr_data = m_questnp->channel[channel].csr_high;
    DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet: csr_data = 0x" << HEX(csr_data));
    num_in_fifo = (csr_data >> 32) & MASK_TX_FIFO_FILL;
    can_write = MAX_TX_FIFO_FILL - num_in_fifo;
    
    if(can_write > size_left)
      will_write = size_left;
    else
      will_write = can_write;
    
    if (will_write & 0x1)
      will_write--;
    
    if(debug_cnt < 40)
    {
      DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet: size_left = " << size_left << ", can_write = " << can_write << ", will_write = " << will_write);
      debug_cnt++;
    }
    
    next_index = 0;  
    if (will_write)
    { 
      for (loop = 0; loop < (will_write / 2); loop++)
      {
	m_questnp->channel[channel].data_port_a64[next_index] = (u_long)source[data_index + 1] << 32 | source[data_index];
	data_index += 2;
	next_index = 1 - next_index;  //toggle between data_port_a64[0] and data_port_a64[1]
      }    
    }
    size_left -= will_write;

    // Allow for trhead cancellation here
    pthread_testcancel();
  }
    
  m_questnp->channel[channel].cw_port_b[0] = 0xe0f00000;
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet: Function done");
}


/******************************************************************************/
void QuestNP::send_packet_memcpy(u_int channel, u_int size, const u_int *source)
/******************************************************************************/
{
  u_int size_left, can_write, will_write, data_index;
  u_long csr_data, num_in_fifo;
  void *ptr1, *ptr2;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet_memcpy: Function called with channel = " <<  channel << ", size = " << size << ", source = 0x" << HEX((u_long)source));

  if (size & 0x3)  //size has to be a multiple of 4 bytes
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::send_packet_memcpy: Size is not a multiple of 4 bytes");
    THROW_QUESTNP_EXCEPTION(SIZE_ERROR, "Size is not a multiple of 4 bytes")
  }

  csr_data = m_questnp->channel[channel].csr_low;
  csr_data = csr_data | 0x40;      //setting the "Enable Unpacker" bit
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet_memcpy: Writing 0x" << HEX(csr_data) << " to csr_low of channel " << channel);
  m_questnp->channel[channel].csr_low = csr_data;

  m_questnp->channel[channel].cw_port_a[0] = 0xb0f00000;
  
  size_left = size / 4;
  data_index = 0;

  while (size_left)
  {
    csr_data = m_questnp->channel[channel].csr_high;
    DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet_memcpy: csr_data = 0x" << HEX(csr_data));
    num_in_fifo = (csr_data >> 32) & MASK_TX_FIFO_FILL;
    can_write = MAX_TX_FIFO_FILL - num_in_fifo;
    
    if(can_write > size_left)
      will_write = size_left;
    else
      will_write = can_write;
        
    DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet_memcpy: data_port pointer = " << HEX((void *)&m_questnp->channel[channel].data_port_a32[0]));
    DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet_memcpy: source pointer = " << HEX((void *)&source[data_index]));
    DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet_memcpy: data_index = " << data_index);

    ptr1 = (void *)&m_questnp->channel[channel].data_port_a32[0];
    ptr2 = (void *)&source[data_index];
    memcpy(ptr1, ptr2, will_write * 4);  //dest, source, size

    size_left -= will_write;
    data_index += will_write;
  }
  m_questnp->channel[channel].cw_port_b[0] = 0xe0f00000;
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::send_packet_memcpy: Function done");
}


/*******************************************************************/
u_char QuestNP::i2c_read(u_char slave, u_char device, u_char address)
/*******************************************************************/
{
  u_long i2csc_data;
  u_char data;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_read: Function called with slave = 0x" << HEX((int)slave) << ", device = 0x" << HEX((int)device) << " and address = 0x" << HEX((int)address));

  i2csc_data = address + (1ul << 33) + (1ul << 35) + ((u_long)slave << 40) + ((u_long)device << 56);   
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::i2c_read: i2csc_data(write to register) = 0x" << HEX(i2csc_data));
  m_questnp->i2csc = i2csc_data;
  
  while(1)  //We don't seem to need a watchdog
  {
    i2csc_data = m_questnp->i2csc;
    if (i2csc_data & (1ul << 32))
    {
      DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::i2c_read: i2csc_data(read back) = 0x" << HEX(i2csc_data));
      break;
    }
  }
  
  if (i2csc_data & (3ul << 38))  //Check the error bits 0 and 1
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::i2c_read: ERROR bit 0 or 1 is set");
    THROW_QUESTNP_EXCEPTION(I2C, "I2C ERROR bit 0 or 1 is set")
  }
  
  data = (i2csc_data >> 8 ) & 0xff;
  //DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::i2c_read: data = 0x" << HEX(data));
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_read: Function done");
  return(data);
}


/*******************************************************************************/
void QuestNP::i2c_write(u_char slave, u_char device, u_char address, u_char data)
/*******************************************************************************/
{
  u_long i2csc_data;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_write: function called with device = 0x" << HEX((u_int)device) << " and slave = 0x" << HEX((u_int)slave));
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_write: address = 0x" << HEX((u_int)address) << ", data = 0x" << HEX((u_int)data));
  
  i2csc_data = address + ((u_long)data << 8) + (1ul << 34) + (1ul << 35) + ((u_long)slave << 40) + ((u_long)device << 56);  
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::i2c_write: i2csc_data(write to register) = 0x" << HEX(i2csc_data));
  m_questnp->i2csc = i2csc_data;
  
  while(1)  //We don't seem to need a watchdog
  {
    i2csc_data = m_questnp->i2csc;
    if (i2csc_data & (1ul << 32))
      break;
  }
  
  if (i2csc_data & (0ul << 38))
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::i2c_write: ERROR bit 0 or 1 is set");
    THROW_QUESTNP_EXCEPTION(I2C, "I2C ERROR bit 0 or 1 is set")
  }
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_write: Function done");
}


/***********************************************************************************************************************/
void QuestNP::i2c_double_write(u_char slave, u_char device, u_char address1, u_char data1, u_char address2, u_char data2)
/***********************************************************************************************************************/
{
  u_long i2csc_data;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_double_write: function called with device = 0x" << HEX((u_int)device) << " and slave = 0x" << HEX((u_int)slave));
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_double_write: address1 = 0x" << HEX((u_int)address1) << ", data1 = 0x" << HEX((u_int)data1));
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_double_write: address2 = 0x" << HEX((u_int)address2) << ", data2 = 0x" << HEX((u_int)data2));

  i2csc_data = address1 + ((u_long)data1 << 8) + ((u_long)address2 << 16) + ((u_long)data2 << 24) + (1ul << 34) + (3ul << 35) + ((u_long)slave << 40) + ((u_long)device << 56);   
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::i2c_double_write: i2csc_data(write to register) = 0x" << HEX(i2csc_data));
  m_questnp->i2csc = i2csc_data;
  
  while(1)  //We don't seem to need a watchdog
  {
    i2csc_data = m_questnp->i2csc;
    if (i2csc_data & (1ul << 32))
      break;
  }
  
  if (i2csc_data & (3ul << 38))
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::i2c_double_write: ERROR bit 0 or 1 is set");
    THROW_QUESTNP_EXCEPTION(I2C, "I2C ERROR bit 0 or 1 is set")
  }
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::i2c_double_write: Function done");
}


/************************************************************************/
void QuestNP::get_QSFP_data(u_int qsfp, T_qsfp_monitoring_data *qsfp_data)
/************************************************************************/
{  
  int loop, channel;
  u_char d1, d2, d3, d4, d5, d6, i2cdata, i2cdata2, is2device, i2cslave;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::get_QSFP_data: function called for QSFP " << qsfp);
  if(qsfp == 0)
  {
    i2cslave  = I2C_SLAVE_QSFP_0;
    is2device = I2C_CHAIN_QSFP_0;
  }
  else if(qsfp == 1)
  {
    i2cslave  = I2C_SLAVE_QSFP_1;
    is2device = I2C_CHAIN_QSFP_1;
  }  
  else if(qsfp == 2)
  {
    i2cslave  = I2C_SLAVE_QSFP_2;
    is2device = I2C_CHAIN_QSFP_2;
  }
  else
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QuestNP::get_QSFP_data: parameter qsfp aut of range");
    THROW_QUESTNP_EXCEPTION(QSFP, "parameter qsfp aut of range")
  }

  for (loop = 0; loop < 16; loop++)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::get_QSFP_data: calling i2c_read");
    i2cdata = i2c_read(i2cslave, is2device, I2C_VENDOR_START + loop);
    qsfp_data->vendor_name[loop] = i2cdata;
  }
  qsfp_data->vendor_name[16] = 0;
  
  for (loop = 0; loop < 16; loop++)
  {
    i2cdata = i2c_read(i2cslave, is2device, I2C_PART_START + loop);
    qsfp_data->part_number[loop] = i2cdata;
  }
  qsfp_data->part_number[16] = 0;

  for (loop = 0; loop < 2; loop++)
  {
    i2cdata = i2c_read(i2cslave, is2device, I2C_REVISION_START + loop);
    qsfp_data->revision[loop] = i2cdata;
  }  
  qsfp_data->revision[2] = 0;
   
  for (loop = 0; loop < 16; loop++)
  {
    i2cdata = i2c_read(i2cslave, is2device, I2C_SERIAL_START + loop);
    qsfp_data->serial_number[loop] = i2cdata;
  }
  qsfp_data->serial_number[16] = 0;

  d1 = i2c_read(i2cslave, is2device, I2C_MANUFACTURE_START + 0);
  d2 = i2c_read(i2cslave, is2device, I2C_MANUFACTURE_START + 1);
  d3 = i2c_read(i2cslave, is2device, I2C_MANUFACTURE_START + 2);
  d4 = i2c_read(i2cslave, is2device, I2C_MANUFACTURE_START + 3);
  d5 = i2c_read(i2cslave, is2device, I2C_MANUFACTURE_START + 4);
  d6 = i2c_read(i2cslave, is2device, I2C_MANUFACTURE_START + 5);

  qsfp_data->manufacture_date[0] = d5;
  qsfp_data->manufacture_date[1] = d6;
  qsfp_data->manufacture_date[2] = 0x2d;
  qsfp_data->manufacture_date[3] = d3;
  qsfp_data->manufacture_date[4] = d4;
  qsfp_data->manufacture_date[5] = 0x2d;
  qsfp_data->manufacture_date[6] = 0x32;
  qsfp_data->manufacture_date[7] = 0x30;
  qsfp_data->manufacture_date[8] = d1;
  qsfp_data->manufacture_date[9] = d2;
  qsfp_data->manufacture_date[10] = 0;

  i2cdata = i2c_read(i2cslave, is2device, I2C_TEMPERATURE);
  qsfp_data->temperature = i2cdata;
  
  i2cdata = i2c_read(i2cslave, is2device, I2C_VOLTAGE);
  qsfp_data->voltage = (i2cdata << 8) * 0.0001;

  for (channel = 0; channel < 4; channel++) 
  {
    i2c_write(i2cslave, is2device, I2C_SECOND_PAGE, 0);
    i2cdata = i2c_read(i2cslave, is2device, I2C_BIAS_START + 2 * channel);
    i2cdata2 = i2c_read(i2cslave, is2device, I2C_BIAS_START + 1 + 2 * channel);
    qsfp_data->tx_bias[channel] = ((i2cdata << 8) | i2cdata2) * 0.002;
  } 
  
  for (channel = 0; channel < 4; channel++)
  {
    i2c_write(i2cslave, is2device, I2C_SECOND_PAGE, 0);
    i2cdata = i2c_read(i2cslave, is2device, I2C_POWER_START + 2 * channel);
    i2cdata2 = i2c_read(i2cslave, is2device, I2C_POWER_START + 1 + 2 * channel);
    qsfp_data->rx_power[channel] = ((i2cdata << 8) | i2cdata2) / 10000.0;
  }     
  
  i2c_write(i2cslave, is2device, I2C_SECOND_PAGE, 0);
  i2cdata = i2c_read(i2cslave, is2device, I2C_ERROR_LOSS_START);
  i2cdata2 = i2c_read(i2cslave, is2device, I2C_ERROR_LOSS_START + 1);
  
  qsfp_data->loss_of_rx_signal[0] = i2cdata & 0x1;
  qsfp_data->loss_of_rx_signal[1] = (i2cdata >> 1) & 0x1;
  qsfp_data->loss_of_rx_signal[2] = (i2cdata >> 2) & 0x1;
  qsfp_data->loss_of_rx_signal[3] = (i2cdata >> 3) & 0x1;
  qsfp_data->loss_of_tx_signal[0] = (i2cdata >> 4) & 0x1;
  qsfp_data->loss_of_tx_signal[1] = (i2cdata >> 5) & 0x1;
  qsfp_data->loss_of_tx_signal[2] = (i2cdata >> 6) & 0x1;
  qsfp_data->loss_of_tx_signal[3] = (i2cdata >> 7) & 0x1;
  qsfp_data->tx_error[0]          = i2cdata2 & 0x1;
  qsfp_data->tx_error[1]          = (i2cdata2 >> 1) & 0x1;
  qsfp_data->tx_error[2]          = (i2cdata2 >> 2) & 0x1;
  qsfp_data->tx_error[3]          = (i2cdata2 >> 3) & 0x1;
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::get_QSFP_data: function done");
}


//Note: QUESTNP_configureI2CClock was derived from RobinNPBIST::configureI2CClock
//The sub-functions were also taken from the RobinNPBIST.cpp in the ROSRobinNP package
/**********************************************/
void QuestNP::configure_i2c_clock(float newFreq)
/**********************************************/
{
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::configure_i2c_clock: function called with newFreq = " << newFreq);
    
  i2c_write(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 135, M_RECALL);  
  waitForClockClearance(M_RECALL);
  
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::configure_i2c_clock: Setting clock to default: 212.5 MHz");
  getClockConfig(212.5);

  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::configure_i2c_clock:  Get default clock config based on measured fxtal for desired frequency");
  computeClockConfig(newFreq);

  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::configure_i2c_clock:  Apply new config");
  applyClockConfig();

  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::configure_i2c_clock:  Read back clock config for new frequency");
  getClockConfig(newFreq);
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::configure_i2c_clock: function done");
}


/**********************************************/
void QuestNP::waitForClockClearance(u_char flag)
/**********************************************/
{
  uint8_t reg135 = flag;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::waitForClockClearance: function called with flag = 0x" << HEX((u_int)flag));
  while (reg135 & flag)
  {
    reg135 = i2c_read(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 135);
  }
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::waitForClockClearance: function done");
}


/**************************************/
void QuestNP::getClockConfig(float fout)
/**************************************/
{
  u_char value;
  int i;
  u_int hs_div, n1;
  uint64_t rfreq_int;
  double fdco, fxtal, rfreq_float;

  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::getClockConfig: function called with fout = " << fout);

  value = i2c_read(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 13);
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: Read Value register shift 13 = 0x" << HEX((int)value));

  hs_div = ((value >> 5) & 0x07) + 4;
  n1 = ((u_int)(value & 0x1f)) << 2;

  value = i2c_read(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 14);
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: Read Value register shift 14 = 0x" << HEX((int)value));

  n1 += (value >> 6) & 0x03;
  n1++;

  rfreq_int = ((uint64_t)(value & 0x3f)) << ((uint64_t)32);

  for(i = 0; i < 4; i++)
  {
    value = i2c_read(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, i + 15);
    DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: Read Value register shift " <<  i + 15 << " = 0x" << HEX((int)value));
    rfreq_int |= ((uint64_t)(value)) << ((3 - i) * 8);
  }

  rfreq_float = (double)(rfreq_int / ((double)(1 << 28)));

  if (fout !=0)
  {
    fdco = fout * n1 * hs_div;
    fxtal = fdco / rfreq_float;
  }
  else 
  {
    fdco = 114.285 * rfreq_float;
    fxtal = 114.285;
  }

  m_cfg.n1          = n1;
  m_cfg.hs_div      = hs_div;
  m_cfg.rfreq_int   = rfreq_int;  
  m_cfg.fxtal       = fxtal;

  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: n1                = " << n1);
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: hs_div            = " << hs_div);
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: rfreq_int         = 0x" << HEX(rfreq_int));
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: rfreq_float       = " << rfreq_float);
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: fdco              = " << fdco << " MHz");
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: fxtal             = " << fxtal << " MHz");
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: fdco/(n1*chs_div) = " << fdco/(n1 * hs_div) << " MHz");
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::getClockConfig: function done");
}


/**************************************************/
void QuestNP::computeClockConfig(float newFrequency)
/**************************************************/
{
  int vco_found = 0, n, h;
  double fDCO_new, lastfDCO = 5670.0;

  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::computeClockConfig: function called with newFrequency = " << newFrequency);
  for (n = 1; n <= 128; n++)
  {
    if (n != 1 && (n & 0x1))
      continue;

    for(h = 11; h > 3; h--)
    {
      if (h == 8 || h == 10)
	continue;

      fDCO_new =  newFrequency * h * n;
      if (fDCO_new >= 4850.0 && fDCO_new <= 5670.0)
      {
	vco_found = 1;
	if (fDCO_new < lastfDCO)
	{
	  m_cfg.hs_div      = h;
	  m_cfg.n1          = n;
      double rfreq_float = fDCO_new / m_cfg.fxtal;
	  m_cfg.rfreq_int = (uint64_t)(trunc(rfreq_float*(1<<28)));
	  lastfDCO = fDCO_new;
	}
      }
    }
  }

  if (vco_found == 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::getClockConfig: VCO ERROR");  
    THROW_QUESTNP_EXCEPTION(VCO, "VCO ERROR")
  }
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::computeClockConfig: function done");
}


/******************************/
void QuestNP::applyClockConfig()
/******************************/
{
  u_char val, value, i, freeze_val;
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::applyClockConfig: function called");

  val = i2c_read(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 137);
  val |= FREEZE_DCO;
  
  i2c_write(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 137, val);                                   
  
  // write new oscillator values
  value = ((m_cfg.hs_div - 4) << 5) | ((m_cfg.n1 - 1) >> 2);
  i2c_write(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 13, value);

  value = (((m_cfg.n1 - 1) & 0x03) << 6) | (m_cfg.rfreq_int >> 32);
  i2c_write(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 14, value);

  // addr 15...18: RFREQ[31:0]
  for(i = 0; i <= 3; i++)
  {
    value = ((m_cfg.rfreq_int>>((3 - i) * 8)) & 0xff);
    i2c_write(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 15 + i, value);
  }

  freeze_val = i2c_read(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 137);  
  freeze_val &= 0xef;    // clear FREEZE_DCO bit

  i2c_double_write(I2C_SLAVE_CLOCK, I2C_CHAIN_CLOCK, 137, freeze_val, 135, M_NEWFREQ);
  waitForClockClearance(M_NEWFREQ);           // wait for NewFreq to be deasserted
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::applyClockConfig: function done");
}


/****************************************************/
void QuestNP::get_config(T_quest_config *quest_config)
/****************************************************/
{
  u_long ldata;
  u_int loop;

  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::get_config: function called");
  
  ldata = m_questnp->design_id_l;
  quest_config->minor      = (u_int)ldata & 0xffff;
  quest_config->major      = (u_int)(ldata >> 16) & 0xff;
  quest_config->version    = (u_int)(ldata >> 24) & 0xff;
  quest_config->ncps       = (u_int)(ldata >> 40) & 0xff;
  quest_config->nsubquests = (u_int)(ldata >> 48) & 0xff;
  quest_config->nchanels   = (u_int)(ldata >> 56) & 0xff;
  
  for(loop = 0; loop < 12; loop++)
  {
    ldata = m_questnp->channel[loop].csr_high;
    quest_config->channel[loop].fifo_full_counter   = (u_int)ldata & 0xff;
    quest_config->channel[loop].data_transfer_ratio = (u_int)(ldata >> 8) & 0xff;
    quest_config->channel[loop].link_up             = (u_int)(ldata >> 16) & 0x1;
    quest_config->channel[loop].fifo_full           = (u_int)(ldata >> 17) & 0x1;
    quest_config->channel[loop].link_down_count     = (u_int)(ldata >> 18) & 0xff;
    quest_config->channel[loop].xoff                = (u_int)(ldata >> 26) & 0x1;
    quest_config->channel[loop].rx_sync             = (u_int)(ldata >> 27) & 0x1;
    quest_config->channel[loop].gtx_reset_done      = (u_int)(ldata >> 28) & 0x1;
    quest_config->channel[loop].rx_pll_locked       = (u_int)(ldata >> 29) & 0x1;
    quest_config->channel[loop].tx_fifo_fill        = (u_int)(ldata >> 32) & 0xfff;
    quest_config->channel[loop].channel_number      = (u_int)(ldata >> 56) & 0x1f;
    quest_config->channel[loop].rawdata             = ldata;
  }
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::get_config: function done");
}


/****************************************/
void QuestNP::channel_reset(u_int channel)
/****************************************/
{  
  u_long regdata;

  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::channel_reset: function called for channel " << channel);

  regdata = m_questnp->channel[channel].csr_low;
  m_questnp->channel[channel].csr_low = regdata | 0x23;      //set the Channel reset , zero transition counter andero link down counter bits 
  m_questnp->channel[channel].csr_low = regdata;             //clear the reset bits 
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::channel_reset: function done");
}


/**************************************/
void QuestNP::slink_reset(u_int channel)
/**************************************/
{
  u_long regdata;
  u_int debugcnt;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 10, "QuestNP::slink_reset: function called");
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: Initial state. regdata = 0x" << HEX(m_questnp->channel[channel].csr_low));

  m_questnp->channel[channel].csr_low = 0x8;   //Set HOLA reset
  m_questnp->channel[channel].csr_low = 0x18;  //Set also GTX reset
  
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: reset bits set. regdata = 0x" << HEX(m_questnp->channel[channel].csr_low));

  m_questnp->channel[channel].csr_low = 0x8;   //Release GTX reset
  m_questnp->channel[channel].csr_low = 0x0;   //Release HOLA reset

  debugcnt = 0;
  m_timeout = 0;
  alarm(3);
  
  regdata = m_questnp->channel[channel].csr_high;
  while((regdata & 0x10000) == 0)
  {
    if(debugcnt < 10)
    {
      DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: waiting for S-Link to come up. regdata = 0x" << HEX(regdata));
      debugcnt++;
    }
    regdata = m_questnp->channel[channel].csr_high;
    
    if(m_timeout == 1)  //time-out
    {
      //printf("Alarm seen.....\n");
      alarm(0);  //Stop the alarm
      DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: time out while waiting for the first link up");
      THROW_QUESTNP_EXCEPTION(TIMEOUT, "time out while waiting for the first link up")
    }
    sched_yield();
  }
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: S-Link is up");

  m_questnp->channel[channel].csr_low = 0x4;   //Set UReset


/*
according to Barry we may miss the link-down state. Therefore let's not wait for it.


  m_timeout = 0;
  alarm(3);
  regdata = m_questnp->channel[channel].csr_high;
  while((regdata & 0x10000))
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: waiting for S-Link to go down");
    regdata = m_questnp->channel[channel].csr_high;

    if(m_timeout == 1)  //time-out
    {
      //printf("Alarm seen 2.....\n");
      alarm(0);  //Stop the alarm
      DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: time out while waiting for the first link down");
      THROW_QUESTNP_EXCEPTION(TIMEOUT, "time out while waiting for the first link down")
    }
    sched_yield();
  }
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: S-Link is down");
*/


  m_questnp->channel[channel].csr_low = 0x0;   //Release UReset

  m_timeout = 0;
  alarm(3);
  regdata = m_questnp->channel[channel].csr_high;
  while((regdata & 0x10000) == 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: waiting for S-Link to come up again");
    regdata = m_questnp->channel[channel].csr_high;

    if(m_timeout == 1)  //time-out
    {
      //printf("Alarm seen 3.....\n");
      alarm(0);  //Stop the alarm
      DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: time out while waiting for the second link up");
      THROW_QUESTNP_EXCEPTION(TIMEOUT, "time out while waiting for the second link up")
    }
    sched_yield();
  }
  
  alarm(0);  //Stop the alarm
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QuestNP::slink_reset: S-Link is up again");
}
















