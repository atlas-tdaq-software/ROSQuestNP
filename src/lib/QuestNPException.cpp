/*******************************************************************/
/*                                                                 */
/* This is the C++ source code of the QuestNPException class       */
/*                                                                 */
/* Author: Markus Joos, CERN                                       */
/*                                                                 */
/**C 2018 Ecosoft - Made from at least 80% recycled source code*****/


#include "ROSQuestNP/QuestNPException.h"

QuestNPException::QuestNPException(u_int errorId, std::string errorText) : std::runtime_error(errorText), m_errorId(errorId), m_errorText(errorText)
{
}

/****************************************/
u_int QuestNPException::getErrorId() const
/****************************************/
{
  return m_errorId;
}


/*************************************************************************/
std::ostream & QuestNPException::printErrorMessage(std::ostream &str) const
/*************************************************************************/
{
  str << "Error " << m_errorId << ":  ";
  printDescription(str);
  return str;
}


/************************************************************************/
std::ostream & QuestNPException::printDescription(std::ostream &str) const
/************************************************************************/
{
  str << m_errorText;
  return str;
}


/********************************************************/
const std::string QuestNPException::getDescription() const
/********************************************************/
{
  std::ostringstream str;
  printDescription(str);
  return str.str();
}


/***************************************************************/
std::string QuestNPException::getErrorString(u_int errorId) const
/***************************************************************/
{
  std::string rc;
  switch (errorId)
  {
    /*********************************/
    /*Exceptions of the QuestNP class*/
    /*********************************/
    case IORCC:
	    rc = "Error from the io_rcc library";
	    break;
    case I2C:
	    rc = "I2C error bit 0 or 1 set";
	    break;
    case QSFP:
	    rc = "Error with QSFP device";
	    break;
    case VCO:
	    rc = "Error in the clock setting algorithm";
	    break;
    case ALARM:
	    rc = "Failed to install or remove alarm handler";
	    break;
    case TIMEOUT:
	    rc = "Timeout error";
	    break;
    case SIZE_ERROR:
	    rc = "Size error";
	    break;
    default:
	    rc = "";
	    break;
  }
  return rc;
}
