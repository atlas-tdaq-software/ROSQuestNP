# Support Contacts (In no particular order)

- Markus Joos
- Gordon Crone
- William Panduro Vazquez

# General Reference

General instructions on how to build with CMake for DAQ/HLT software can be found at:

https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltCMake

# Specifc Build Instructions

To build ROSQuestNP, please follow the instructions below:

- Clone a copy of the ROSQuestNP Package into your working directory.
```
git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-software/ROSQuestNP
```

- Create a top level CMakeLists.txt file in your working directory.

    The file should contain the following (assuming you want to build against tdaq-07-01-00): 
```
cmake_minimum_required(VERSION 3.6.0)
find_package(TDAQ)
include(CTest)
tdaq_work_area(tdaq-07-01-00)
```

- Set Up TDAQ release environment.

    For example, for tdaq-07-01-00 in SLC6:
```
source /afs/cern.ch/atlas/project/tdaq/cmake/cmake_tdaq/bin/cm_setup.sh tdaq-07-01-00 x86_64-slc6-gcc62-opt
```

- Configure CMake for your environment.

    For example, for SLC6:
```
cmake_config x86_64-slc6-gcc62-opt
```

- Build package (having made any necessary edits).
```
cd x86_64-slc6-gcc62-opt
make install
```

- Link to or run libraries and binaries.

    These are to be found in the 'installed' folder in your work directory.
    Your `LD_LIBRARY_PATH` and `PATH` environment variables should be modified accordingly.